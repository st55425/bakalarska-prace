package exercise.utils;

import exercise.Fraction;
import org.ejml.simple.SimpleMatrix;

import java.util.Arrays;
import java.util.Random;

public final class Generator {

    private Generator(){}

    public static SimpleMatrix generateSimpleMatrix(int numberOfRows, int numberOfColumns, int min, int max){
        return new SimpleMatrix(generateDoubleMatrix(numberOfRows, numberOfColumns,min,max));
    }

    public static double[][] generateDoubleMatrix(int numberOfRows, int numberOfColumns, int min, int max){
        double[][] matrix = new double[numberOfRows][numberOfColumns];
        for (int i = 0; i < numberOfRows; i++) {
            matrix[i] = generateDoubleVector(numberOfColumns, min, max);
        }
        return matrix;
    }

    public static SimpleMatrix generateRegularMatrix(int numberOfRows, int min, int max){
        SimpleMatrix matrix = new SimpleMatrix(generateSimpleMatrix(numberOfRows, numberOfRows, min, max));

        while (matrix.determinant() == 0) {
            matrix = new SimpleMatrix(generateSimpleMatrix(numberOfRows, numberOfRows, min, max));
        }
        return matrix;
    }

    public static double[] generateDoubleVector(int numberOfRows, int min, int max){
        Random random = new Random();
        boolean allZeros = true;
        double[] vector = new double[numberOfRows];
        while (allZeros){
            for (int i = 0; i < numberOfRows; i++) {
                vector[i] = random.nextInt(max +1 - min) + min;
                if (vector[i] != 0){
                    allZeros = false;
                }
            }
        }
        return vector;
    }

    public static SimpleMatrix generateUnimodularMatrix(int numberOfRows, int min, int max){
        Random random = new Random();
        double[][] matrix = generateUnitDoubleMatrix(numberOfRows);
        for (int i = 0; i < numberOfRows-1; i++) {
            for (int j = i+1; j < numberOfRows; j++) {
                matrix[i][j] = random.nextInt(max +1 - min) + min;
            }
        }

        for (int i = 0; i < 100; i++) {
            int firstRow = random.nextInt(numberOfRows);
            int secondRow = random.nextInt(numberOfRows);
            if (firstRow != secondRow){
                if (i%2 ==1){
                    MathFunctions.sumMatrixRows(matrix, firstRow, secondRow,1);
                }else{
                    MathFunctions.subtractMatrixRows(matrix, firstRow, secondRow,1);
                }

            }
        }
        if (!MathFunctions.reduceNumbersInMatrix(matrix, min, max, 0)){
            return generateUnimodularMatrix(numberOfRows, min, max);
        }

        return new SimpleMatrix(matrix);
    }

    public static double[][] generateUnitDoubleMatrix(int numberOfRows){
        double[][] matrix = new double[numberOfRows][numberOfRows];
        for (int i = 0; i < numberOfRows; i++) {
            matrix[i][i] = 1;
        }
        return matrix;
    }

    public static Fraction[] generateFractionArray(int length, double value){
        Fraction[] array = new Fraction[length];
        for (int i = 0; i < length; i++) {
            array[i] = new Fraction(value);
        }
        return array;
    }

    public static Fraction[][] generateFractionMatrix(int numberOfRows, int numberOfColumns, double defaultValue){
        Fraction[][] matrix = new Fraction[numberOfRows][numberOfColumns];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = generateFractionArray(numberOfColumns, defaultValue);
        }
        return matrix;
    }

    public static Fraction[][] generateSingularMatrix(int numberOfRows, int min, int max, int offset){
        if (numberOfRows - offset <=1){
            throw new IllegalArgumentException("počet řádků je moc malý a/nebo offset je moc veliký");
        }
        Random random = new Random();
        double[][] matrix = MathFunctions.matrixFromArray(generateRegularMatrix(numberOfRows, min/2, max/2).getDDRM().data,
                 numberOfRows);
        int selectedVector = 0;
        int finishVector = 0;
        while (selectedVector == finishVector){
            selectedVector = random.nextInt(numberOfRows-offset);
            finishVector = random.nextInt(numberOfRows-offset);
        }
        matrix[finishVector] = Arrays.copyOf(matrix[selectedVector], matrix[0].length);
        for (int i = 0; i < 1155; i++) {
            if (random.nextBoolean()){
                MathFunctions.sumMatrixRows(matrix, random.nextInt(numberOfRows-offset), finishVector, random.nextInt(max)+1);
            }else {
                MathFunctions.subtractMatrixRows(matrix, random.nextInt(numberOfRows-offset), finishVector, random.nextInt(max)+1);
            }
        }
        if (!MathFunctions.reduceNumbersInMatrix(matrix, -max, max,1)){
            return generateSingularMatrix(numberOfRows, min, max, offset);
        }
        double[] zeroes = new double[numberOfRows];
        for (int i = 0; i < matrix.length; i++) {
            if (Arrays.equals(zeroes, matrix[i]) ||(i != finishVector && Arrays.equals(matrix[i], matrix[finishVector]))) {
                return generateSingularMatrix(numberOfRows, min, max, offset);
            }
        }



        return MathFunctions.clone2DArray(matrix);
    }

}
