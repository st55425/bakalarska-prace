package exercise.utils;

import exercise.Fraction;


public final class StringCreator {

    private StringCreator(){}

    public static String matrixToString(Fraction[][] matrix){
        StringBuilder string = new StringBuilder();
        for (Fraction[] fractions : matrix) {
            for (Fraction fraction : fractions) {
                if (fraction != null){
                    string.append(fraction.toString());
                    string.append(" ");
                }
            }
            string.append("\n");
        }
        return string.toString();
    }

    public static String nullNumberInRowExplanation(int nullableRow, int controlRow, Fraction controlNumber, Fraction nullableNumber){
        String explanation;
        if (Math.signum(nullableNumber.toDouble()) == Math.signum(controlNumber.toDouble())){
            if (controlNumber.isInteger() && Math.abs(controlNumber.getNumerator()) == 1){
                explanation = "Od "+ (nullableRow + 1) + ". řádku odečteme ";
            }
            else if(controlNumber.isInteger()) {
                explanation = "Od " + (int)Math.abs(controlNumber.toDouble()) + "násobku " + (nullableRow + 1) + ". řádku odečteme ";
            }
            else {
                explanation = "Od " + Math.abs(controlNumber.toDouble()) + "násobku " + (nullableRow + 1) + ". řádku odečteme ";
            }
        }
        else {
            if (controlNumber.isInteger() && Math.abs(controlNumber.getNumerator()) == 1){
                explanation = "K "+ (nullableRow + 1) + ". řádku přičteme ";
            }
            else if(controlNumber.isInteger()){
                explanation = "K " + (int)Math.abs(controlNumber.toDouble())+ "násobku " + (nullableRow + 1) + ". řádku přičteme ";
            }
            else {
                explanation = "K " + Math.abs(controlNumber.toDouble()) + "násobku " + (nullableRow + 1) + ". řádku přičteme ";
            }
        }
        if (nullableNumber.isInteger() && Math.abs(nullableNumber.getNumerator()) == 1){
            explanation +=  + (controlRow + 1) + ". řádek.";
        }
        else if(nullableNumber.isInteger()){
            explanation += (int)Math.abs(nullableNumber.toDouble()) + "násobek " + (controlRow + 1) + ". řádku.";
        }
        else {
            explanation += Math.abs(nullableNumber.toDouble())+ "násobek " + (controlRow + 1) + ". řádku.";
        }

        return explanation;
    }
}
