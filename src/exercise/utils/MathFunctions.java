package exercise.utils;

import exercise.Fraction;
import exercise.ParametricFraction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Random;

public final class MathFunctions {

    private MathFunctions(){}

    public static double[][] clone2DArrayToDouble(Fraction[][] array){
        double[][] newArray = new double[array.length][array[0].length];
        for(int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                newArray[i][j] = array[i][j].toDouble();
            }
        }
        return newArray;
    }

    public static Fraction[][] clone2DArray(double[][] array){
        Fraction[][] newArray = new Fraction[array.length][array[0].length];
        for(int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                newArray[i][j] = new Fraction(array[i][j]);
            }
        }
        return newArray;
    }

    public static Fraction[][] clone2DArray(Fraction[][] array){
        Fraction[][] newArray = new Fraction[array.length][array[0].length];
        for(int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                newArray[i][j] =copyFraction(array[i][j]);
            }
        }
        return newArray;
    }

    public static double[][] matrixFromArray(double[] array, int numberOfColumns){
        int numberOfRows = array.length/numberOfColumns;
        double[][] matrix = new double[numberOfRows][numberOfColumns];
        for (int i = 0; i < numberOfRows; i++){
            System.arraycopy(array, i * numberOfColumns, matrix[i], 0, numberOfColumns);
        }
        return matrix;
    }

    public static Fraction[][] matrixFromArray(double[] array, int numberOfRows, int numberOfColumns){
         Fraction[][] matrix = new Fraction[numberOfRows][numberOfColumns];
        for (int i = 0; i < numberOfRows; i++){
            for (int j = 0; j < numberOfColumns; j++){
                matrix[i][j] = new Fraction(array[i*numberOfColumns+j]);
            }
        }
        return matrix;
    }

    public static Fraction[][] matrixFromArray(Fraction[] array, int numberOfRows, int numberOfColumns){
        Fraction[][] matrix = new Fraction[numberOfRows][numberOfColumns];
        for (int i = 0; i < numberOfRows; i++){
            for (int j = 0; j < numberOfColumns; j++){
                matrix[i][j] = copyFraction(array[i*numberOfColumns+j]);
            }
        }
        return matrix;
    }

    public static double[][] clone2DArray(double[][] array, int numberOfRows, int numberOfColumns, int rowsOffset, int columnOffset){
        double[][] newArray = new double[numberOfRows][numberOfColumns];
        for(int i = rowsOffset; i < Math.min(array.length, numberOfRows+rowsOffset); i++) {
            for(int j = columnOffset; j < Math.min(array[i].length, numberOfColumns+columnOffset); j++) {
                newArray[i-rowsOffset][j-columnOffset] = array[i][j];
            }
        }
        return newArray;
    }

    public static Fraction[][] clone2DArray(Fraction[][] array, int numberOfRows, int numberOfColumns, int rowsOffset, int columnOffset){
        Fraction[][] newArray = Generator.generateFractionMatrix(numberOfRows, numberOfColumns, 0);
        for(int i = rowsOffset; i < Math.min(array.length, numberOfRows+rowsOffset); i++) {
            for(int j = columnOffset; j < Math.min(array[i].length, numberOfColumns+columnOffset); j++) {
                newArray[i-rowsOffset][j-columnOffset] = copyFraction(array[i][j]);
            }
        }
        return newArray;
    }

    public static Fraction[] cloneArray(Fraction[] array){
        Fraction[] newArray = new Fraction[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = copyFraction(array[i]);
        }
        return newArray;
    }


    public static void switchRows2DArray(Fraction[][] array, int firstRow, int secondRow){
        Fraction[] tmp = cloneArray(array[firstRow]);
        array[firstRow] = cloneArray(array[secondRow]);
        array[secondRow] = tmp;
    }


    public static void divideRow2DArray(Fraction[][] array, int row, double divisor){
        for (int i = 0; i < array[0].length; i++) {
            if (array[row][i] instanceof ParametricFraction && ((ParametricFraction)array[row][i]).getParameterName().equals("=")){
                continue;
            }
            array[row][i] = array[row][i].divide(new Fraction(divisor));
        }
    }

    public static void divideRow2DArray(Fraction[][] array, int row, Fraction divisor){
        for (int i = 0; i < array[0].length; i++) {
            array[row][i] = array[row][i].divide(divisor);
        }
    }

    public static int countNonZeroFractions(Fraction[] array, int rightSideOffset){
        int counter = 0;
        for (int i = 0; i < array.length-rightSideOffset; i++) {
            if (array[i].getNumerator() != 0) {
                counter++;
            }
        }
        return counter;
    }

    public static String nullNumberInMatrix(Fraction[][] matrix, int controlRow, int nullableRow, int positionInRow){
        Fraction controlNumber = matrix[controlRow][positionInRow];
        Fraction nullableNumber = matrix[nullableRow][positionInRow];

        if (controlNumber.getDenominator() == nullableNumber.getDenominator()){
            int gcd = MathFunctions.calculateGCD(controlNumber.getNumerator(), nullableNumber.getNumerator());
            controlNumber = new Fraction(controlNumber.getNumerator()/gcd, controlNumber.getDenominator());
            nullableNumber = new Fraction(nullableNumber.getNumerator()/gcd, controlNumber.getDenominator());
        }

        for (int i = 0; i < matrix[0].length; i++) {
            Fraction nullable = matrix[nullableRow][i].multiply(controlNumber.abs());
            Fraction control = nullableNumber.abs().multiply(matrix[controlRow][i]);

            if (Math.signum(nullableNumber.getNumerator()) != Math.signum(controlNumber.getNumerator())){
                matrix[nullableRow][i] = nullable.add(control);
            }else{
                matrix[nullableRow][i] = nullable.subtract(control);
            }
        }

        return StringCreator.nullNumberInRowExplanation(nullableRow, controlRow, controlNumber, nullableNumber);
    }

    public static boolean isInteger(double value, int precision){
        if (precision < 0) throw new IllegalArgumentException();

        BigDecimal rounded = new BigDecimal(Double.toString(value));
        rounded = rounded.setScale(precision, RoundingMode.HALF_UP);
        BigDecimal integer = rounded.setScale(0, RoundingMode.HALF_UP);

        return integer.compareTo(rounded) == 0;
    }

    public static String switchMinRowByColumn(Fraction[][] matrix, int startRow){
        if (startRow > matrix.length-1){
            return "";
        }
        int column = Arrays.stream(MathFunctions.calculateZeroFractions(matrix, startRow)).min().getAsInt();
        int index = startRow;
        Fraction minimum = new Fraction(matrix[startRow][column]);
        for (int i = startRow+1; i < matrix.length; i++){
            if (matrix[i][column].toDouble() != 0 && (minimum.toDouble()==0 || Math.abs(matrix[i][column].toDouble()) < Math.abs(minimum.toDouble()))){
                minimum = matrix[i][column];
                index = i;
            }
        }
        if (index == startRow){
            return "";
        } else {
            switchRows2DArray(matrix, index, startRow);
            if (matrix[index][column].getNumerator()==0){
                return "Abychom mohli vytvořit matici ve schodovitém tvaru, prohodíme "+ (startRow+1) + ". a "+ (index+1) + ". řádek. ";
            }else {
                return "Pro zjednodušení výpočtu prohodíme "+ (startRow+1) + ". a "+ (index+1) + ". řádek. ";
            }
        }
    }

    public static String switchZeroFromPivot(Fraction[][] matrix, int row){
        if (matrix.length ==1 || row > matrix.length-1){
            return "";
        }
        int[] nullFractions = calculateZeroFractions(matrix, row);
        int min = nullFractions[0];
        int index = row;
        boolean switchBecauseZero = true;
        for (int i = 1; i < nullFractions.length; i++) {
            if (nullFractions[i] < min){
                min = nullFractions[i];
                index = row+i;
                switchBecauseZero = true;
            } else if (nullFractions[i] == min && Math.abs(matrix[row][min].toDouble()) != 1 && Math.abs(matrix[row+i][min].toDouble()) == 1){
                index = row +i;
                switchBecauseZero = false;
            }
        }

        if (index == row){
            return "";
        }
        switchRows2DArray(matrix, row, index);
        if (switchBecauseZero){
            return "Abychom mohli vytvořit matici ve schodovitém tvaru, prohodíme "+ (row+1) + ". a "+ (index+1) + ". řádek.";
        }else {
            return "Pro zjednodušení výpočtu prohodíme "+ (row+1) + ". a "+ (index+1) + ". řádek. ";
        }
    }

    private static int[] calculateZeroFractions(Fraction[][] matrix, int startRow){
        if (startRow > matrix.length-1){
            throw new IllegalArgumentException("startovní řada neodpovídá velikosti matice");
        }
        int[] nullFractions = new int[matrix.length-startRow];
        for (int i = startRow; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j].getNumerator() != 0){
                    nullFractions[i-startRow] = j;
                    break;
                }
            }
        }
        return nullFractions;
    }

    public static int calculateGCD(int first, int second){
        BigInteger b1 = BigInteger.valueOf(first);
        BigInteger b2 = BigInteger.valueOf(second);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }

    public static int calculateLCM(int first, int second) {
        return Math.abs((first*second)/calculateGCD(first, second));
    }

    public static boolean findLcmRowsByColumn(Fraction[][] matrix,int row, int column, boolean down, int[] result){
        Fraction lcm = new Fraction(Integer.MAX_VALUE);
        int from = 0;
        int to = Math.max(matrix.length, column +1 );
        if (down){
            from = row;
            to = matrix.length;
        }
        for (int i = from; i < to; i++) {
            for (int j = 0; j < matrix.length; j++) {
                Fraction tmp = new Fraction(Integer.MAX_VALUE);
                if (i >= j || matrix[i][column].toDouble() == 0 || matrix[j][column].toDouble() ==0){
                    continue;
                }
                if (Math.abs(matrix[i][column].toDouble()) == Math.abs(matrix[j][column].toDouble())) {
                    result[0] = i;
                    result[1] = j;
                    return true;
                }
                if (matrix[i][column].isInteger() && matrix[j][column].isInteger()){
                    tmp = new Fraction(calculateLCM(matrix[i][column].getNumerator(), matrix[j][column].getNumerator()));
                }

                if (tmp.toDouble() < lcm.toDouble()){
                    lcm = tmp;
                    result[0] = Math.min(i,j);
                    result[1]= Math.max(i,j);
                }
            }
        }
        return result[0] != result[1];
    }

    public static boolean findIdenticalRows(Fraction[][] matrix, int rightSideCount, int[] result){
        Fraction[][] newMatrix = clone2DArray(matrix, matrix.length, matrix[0].length-rightSideCount, 0,0);
        for (int i = 0; i < newMatrix.length; i++) {
            for (int j = 0; j < newMatrix.length; j++) {
                if (i < j && Arrays.equals(newMatrix[i], newMatrix[j])){
                    result[0] = i;
                    result[1] = j;
                    return true;
                }
            }
        }
        return false;
    }

    public static int findFirstNonZeroFraction(Fraction[] array){
        for (int i = 0; i < array.length; i++) {
            if (array[i].getNumerator() != 0) {
                return i;
            }
        }
        return 0;
    }

    public static boolean changeSignumOfRow(Fraction[][] matrix, int row){
        for (int i = 0; i < matrix[row].length; i++) {
            if (matrix[row][i].getNumerator() != 0) {
                return matrix[row][i].toDouble() < 0;
            }
        }
        return false;
    }

    public static int divisorOfRow(Fraction[][] matrix, int row){
        int gcd = isInteger(matrix[row][0].toDouble(), 10) ? matrix[row][0].getNumerator() : 1 ;
        for (int i = 1; i < matrix[row].length; i++) {
            if (gcd != 1 && isInteger(matrix[row][i].toDouble(), 10)){
                gcd = calculateGCD(gcd, matrix[row][i].getNumerator());
            }else {
                return 1;
            }
        }
        return gcd;
    }

    public static void sumMatrixRows(double[][] matrix, int controlRow, int mutableRow, int coefficient){
        for (int i = 0; i < matrix[mutableRow].length; i++) {
            matrix[mutableRow][i] += matrix[controlRow][i]*coefficient;
        }
    }

    public static void subtractMatrixRows(double[][] matrix, int controlRow, int mutableRow, int coefficient){
        for (int i = 0; i < matrix[mutableRow].length; i++) {
            matrix[mutableRow][i] -= matrix[controlRow][i]*coefficient;
        }
    }

    public static void subtractMatrixRows(Fraction[][] matrix, int controlRow, int mutableRow){
        for (int i = 0; i < matrix[mutableRow].length; i++) {
            matrix[mutableRow][i] = matrix[mutableRow][i].subtract(matrix[controlRow][i]);
        }
    }

    public static double findMaximumInMatrix(double[][] matrix, int[] position){
         double maximum = Double.MIN_VALUE;
         for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] > maximum){
                    maximum = matrix[i][j];
                    position[0] = i;
                    position[1] = j;
                }
            }
        }
         return maximum;
    }

    public static double findAbsMaximumNumberInMatrix(Fraction[][] matrix){
        double maximum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (Math.abs(matrix[i][j].getNumerator()) > maximum){
                    maximum = Math.abs(matrix[i][j].getNumerator());
                }
                if (Math.abs(matrix[i][j].getDenominator()) > maximum){
                    maximum = Math.abs(matrix[i][j].getDenominator());
                }
            }
        }
        return maximum;
    }

    public static double findMinimumInMatrix(double[][] matrix, int[] position){
        double minimum = Double.MAX_VALUE;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] < minimum){
                    minimum = matrix[i][j];
                    position[0] = i;
                    position[1] = j;
                }
            }
        }
        return minimum;
    }

    public static Fraction[][] transponeMatrix(Fraction[][] matrix){
        Fraction[][] newMatrix = new Fraction[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                newMatrix[i][j] = copyFraction(matrix[j][i]);
            }
        }
        return newMatrix;
    }

    public static Fraction copyFraction(Fraction fraction){
        if (fraction != null){
            if (fraction instanceof ParametricFraction){
                return new ParametricFraction(fraction);
            }
            return new Fraction(fraction);
        }
        return null;
    }

    public static boolean reduceNumbersInMatrix(double[][] matrix, double min, double max, int offset){
        if (matrix.length - offset <= 0){
            throw new IllegalArgumentException("offset je moc veliký");
        }
        Random random = new Random();
        int[] position = new int[2];
        boolean changed = true;
        int index = 0;
        while (changed){
            changed = false;
            if (MathFunctions.findMaximumInMatrix(matrix, position) > max || index <100){
                int randomRow;
                do {
                    randomRow = random.nextInt(matrix.length - offset);
                }
                while (position[0] == randomRow);
                if (matrix[randomRow][position[1]] >0) {
                    MathFunctions.subtractMatrixRows(matrix, randomRow, position[0],1);
                }else {
                    MathFunctions.sumMatrixRows(matrix, randomRow, position[0],1);
                }
                changed = true;
            }
            if (MathFunctions.findMinimumInMatrix(matrix, position) < min || index <100){
                int randomRow;
                do {
                    randomRow = random.nextInt(matrix.length - offset);
                }
                while (position[0] == randomRow);
                if (matrix[randomRow][position[1]] < 0) {
                    MathFunctions.subtractMatrixRows(matrix, randomRow, position[0],1);
                }else {
                    MathFunctions.sumMatrixRows(matrix, randomRow, position[0],1);
                }
                changed = true;
            }
            if (index > 1000){
                return false;
            }
            index++;
        }
        return true;
    }

}
