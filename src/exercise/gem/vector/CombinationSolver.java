package exercise.gem.vector;

import exercise.Fraction;
import exercise.MatrixSolver;
import exercise.MatrixStep;
import exercise.ParametricFraction;
import exercise.gem.GEMSolver;
import exercise.utils.MathFunctions;

import java.util.ArrayList;
import java.util.List;

public class CombinationSolver extends MatrixSolver {

    public CombinationSolver(MatrixStep task) {
        super(task);
        makeSolution();
    }

    private void makeSolution(){
        removeVectorNames();
        GEMSolver solver = new GEMSolver(solution, 1);
        callAnotherSolver(solver);
        finishSolution(solver);
    }

    private void removeVectorNames(){
        Fraction[][] firstStep = MathFunctions.clone2DArray(lastMatrix, lastMatrix.length,
                lastMatrix[0].length-2, 0,2);
        addStep(new MatrixStep(MathFunctions.transponeMatrix(firstStep), "Z vektorů vytvoříme rozšířenou matici soustavy rovnic definující lineární kombinaci vektorů."));
    }

    private void finishSolution(GEMSolver solver){
        int numberOfVectors = solver.getSolvedVariables().size();
        String vectorName = String.valueOf((char)('A'+ numberOfVectors));

        if (solver.isSolvable()){
            List<Fraction> solutionList = new ArrayList<>();
            Fraction[][] solution = new Fraction[1][1];
            solutionList.add(new ParametricFraction(1, vectorName));
            solutionList.add(new ParametricFraction(1, "="));
            for (int i = 0; i < numberOfVectors; i++) {
                solutionList.addAll(solver.getSolvedVariables().get(GEMSolver.VARIABLES[i]));
                solutionList.add(new ParametricFraction(1, String.valueOf((char)('A'+ i))));
            }
            solution[0] = solutionList.toArray(solution[0]);
            addStep(new MatrixStep(solution, "Vektor " + vectorName + " je lineární kombinací ostatních vektorů."));

        }else {
            Fraction[][] solution = new Fraction[1][1];
            solution[0][0] = new ParametricFraction(1, "Není LK");
            addStep(new MatrixStep(solution, "Vektor " + vectorName + " není lineární kombinací ostatních vektorů."));
        }
    }
}
