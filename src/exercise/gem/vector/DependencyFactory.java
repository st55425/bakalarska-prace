package exercise.gem.vector;

import exercise.Fraction;
import exercise.IExercise;
import exercise.MatrixStep;
import exercise.ParametricFraction;
import exercise.utils.Generator;
import exercise.utils.MathFunctions;
import org.ejml.simple.SimpleMatrix;

import java.util.Random;

public final class DependencyFactory {

    private DependencyFactory(){}

    public static IExercise generateExercise(int numberOfVectors, int max){
        MatrixStep task;
        do {
            Fraction[][] matrix = MathFunctions.clone2DArray(generateExerciseMatrix(numberOfVectors, max),
                    numberOfVectors, numberOfVectors+2,0,0);
            for (int i = 0; i < numberOfVectors; i++) {
                matrix[i][numberOfVectors+1] = matrix[i][0];
                matrix[i][numberOfVectors] = matrix[i][1];
                char parameter = (char)('A'+i);
                matrix[i][0] = new ParametricFraction(1,Character.toString(parameter));
                matrix[i][1] = new ParametricFraction(1,"=");
            }
            task = new MatrixStep(matrix, "Určete, zda jsou vektory lineárně nezávislé.");
        }while (new DependencySolver(task).getMaxAbsNumberInSolution() > Math.sqrt(Math.pow(max, numberOfVectors)));

        return new DependencyExercise(task);
    }

    private static Fraction[][] generateExerciseMatrix(int numberOfVectors, int max){
        Random random = new Random();
        if (random.nextBoolean()){
            SimpleMatrix taskMatrix = Generator.generateRegularMatrix(numberOfVectors, -max, max);
            return MathFunctions.matrixFromArray(taskMatrix.getDDRM().data, numberOfVectors, numberOfVectors);
        }else {
            Fraction[][] matrix = Generator.generateSingularMatrix(numberOfVectors, -max, max,1);
            return MathFunctions.transponeMatrix(matrix);
        }
    }
}
