package exercise.gem.vector;

import exercise.Fraction;
import exercise.IExercise;
import exercise.MatrixStep;
import exercise.ParametricFraction;
import exercise.gem.GEMFactory;
import exercise.utils.Generator;
import exercise.utils.MathFunctions;
import org.ejml.simple.SimpleMatrix;

import java.util.Random;

public final class CombinationFactory {

    private CombinationFactory() {
    }

    public static IExercise generateExercise(int numberOfVectors, int max){
        String vectorName = String.valueOf((char)('A'+ numberOfVectors-1));
        MatrixStep task;
        do {
            Fraction[][] matrix = MathFunctions.clone2DArray(generateExerciseMatrix(numberOfVectors, max),
                    numberOfVectors, numberOfVectors+1,0,0);
            for (int i = 0; i < numberOfVectors; i++) {
                matrix[i][numberOfVectors] = matrix[i][0];
                matrix[i][numberOfVectors-1] = matrix[i][1];
                char parameter = (char)('A'+i);
                matrix[i][0] = new ParametricFraction(1,Character.toString(parameter));
                matrix[i][1] = new ParametricFraction(1,"=");
            }
            task = new MatrixStep(matrix, "Určete, zda je vektor "
                    + vectorName + " lineární kombinací ostatních vektorů.");
        }while (new CombinationSolver(task).getMaxAbsNumberInSolution() > Math.sqrt(Math.pow(max, numberOfVectors)));

        return new CombinationExercise(task);
    }


    private static Fraction[][] generateExerciseMatrix(int numberOfVectors, int max){
        Random random = new Random();
        if (random.nextBoolean()){
            SimpleMatrix taskMatrix = GEMFactory.generateGoodExercise(numberOfVectors-1, max);
            return MathFunctions.matrixFromArray(taskMatrix.transpose().getDDRM().data, numberOfVectors, numberOfVectors-1);
        }else {
            Fraction[][] matrix = Generator.generateSingularMatrix(numberOfVectors, -max, max,1);
            return MathFunctions.clone2DArray(matrix, numberOfVectors, numberOfVectors-1, 0,0);
        }
    }
}
