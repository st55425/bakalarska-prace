package exercise.gem.vector;

import exercise.*;

public class DependencyExercise extends MatrixExercise {

    public DependencyExercise(MatrixStep task) {
        super(task);
        solver = new DependencySolver(task);
    }


    @Override
    public Type getType() {
        return Type.DEPENDENCY;
    }
}
