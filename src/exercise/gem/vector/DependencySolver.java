package exercise.gem.vector;

import exercise.Fraction;
import exercise.MatrixSolver;
import exercise.MatrixStep;
import exercise.ParametricFraction;
import exercise.gem.GEMSolver;
import exercise.utils.MathFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DependencySolver extends MatrixSolver {

    public DependencySolver(MatrixStep task) {
        super(task);
        makeSolution();
    }


    private void makeSolution(){
        removeVectorNames();
        GEMSolver solver = new GEMSolver(solution, 1);
        callAnotherSolver(solver);
        List<Fraction> variables = rewriteToEquations(solver);
        finishSolution(variables);
    }

    private void removeVectorNames(){
        Fraction[][] firstStep = MathFunctions.clone2DArray(lastMatrix, lastMatrix.length+1,
                lastMatrix.length, 0,2);
        addStep(new MatrixStep(MathFunctions.transponeMatrix(firstStep), "Z vektorů vytvoříme rozšířenou matici soustavy rovnic definující lineární závislost vektorů."));
    }

    private List<Fraction> rewriteToEquations(GEMSolver solver){
        HashMap<String, List<Fraction>> variables = solver.getSolvedVariables();
        List<Fraction> equation = new ArrayList<>();
        List<Fraction> variablesList = new ArrayList<>();
        Fraction[][] solution = new Fraction[1][1];
        for (int i = 0; i < variables.size(); i++) {
            equation.addAll(variables.get(GEMSolver.VARIABLES[i]));
            variablesList.addAll(variables.get(GEMSolver.VARIABLES[i]));
            equation.add(new ParametricFraction(1, String.valueOf((char)('A'+ i))));
        }
        equation.add(new ParametricFraction(1, "="));
        equation.add(new Fraction(0));
        solution[0] = equation.toArray(solution[0]);
        addStep(new MatrixStep(solution, "Dosadíme řešení do rovnice."));

        return variablesList;
    }

    private void finishSolution(List<Fraction> variables){
        Fraction[][] solution = new Fraction[1][1];
        if (variables.stream().allMatch(i -> i.getNumerator() ==0)){
            solution[0][0] = new ParametricFraction(1, "Nejsou LZ");
            addStep(new MatrixStep(solution, "Všechny parametry se rovnají nule -> vektory nejsou lineárně závislé."));
        }else {
            solution[0][0] = new ParametricFraction(1, "Jsou LZ");
            addStep(new MatrixStep(solution, "Rovnice obsahuje nenulové parametry -> vektory jsou lineárně závislé."));
        }
    }

}
