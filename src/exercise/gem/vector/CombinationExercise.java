package exercise.gem.vector;

import exercise.*;

public class CombinationExercise extends MatrixExercise {

    public CombinationExercise(MatrixStep task) {
        super(task);
        solver = new CombinationSolver(task);
    }

    @Override
    public Type getType() {
        return Type.COMBINATION;
    }
}
