package exercise.gem;

import exercise.*;
import exercise.utils.MathFunctions;
import exercise.utils.StringCreator;

import java.util.*;


public class GEMSolver extends MatrixSolver {

    public static final String[] VARIABLES = new String[]{"x", "y","z", "u", "v", "w", "a", "b", "c"};


    private int eliminatedLinesCount;
    private boolean regular;
    private boolean solvable;
    private final int rightSideCount;
    private HashMap<String, List<Fraction>> solvedVariables;


    public GEMSolver(MatrixStep task, int rightSideCount) {
        super(task);
        regular = true;
        eliminatedLinesCount = 0;
        solvable = true;
        solvedVariables = new HashMap<>();
        this.rightSideCount = rightSideCount;
        makeSolution();
    }

    public HashMap<String, List<Fraction>> getSolvedVariables(){
        return solvedVariables;
    }

    public boolean isSolvable(){
        return solvable;
    }

    public boolean isRegular(){
        return regular;
    }

    private void makeSolution() {

        rewriteToMatrix();

        eliminatedLinesCount = lastMatrix[0].length - lastMatrix.length -rightSideCount;
        if (eliminatedLinesCount > 0){
            regular = false;
        }

        for (int i = 0; i < lastMatrix.length; i++) {
            if (checkZeroedRow(i)){
                divideRowSteps(i);
            }else {
                i--;
            }
            if (!solvable) return;
        }

        for (int i = 0; i < lastMatrix.length; i++) {
            removeAllIdenticalRows();
            switchZeroFromPivot(i);
            if (!solvable) return;
            int column = MathFunctions.findFirstNonZeroFraction(lastMatrix[i]);
            for (int j = i+1; j < lastMatrix.length; j++) {
                int[] rows = new int[2];
                if (!solvable){
                    return;
                }
                if (lastMatrix.length > 1 && MathFunctions.findLcmRowsByColumn(lastMatrix, i, column, true, rows)) {
                    if (createSteps(rows[0], rows[1], column)){
                        j--;
                    }
                }
            }
        }
        if (!solvable){
            return;
        }

        if (regular) {
            makeUnitSolution();
        }
        else {
            createParametricSolution();
        }
    }

    private void rewriteToMatrix(){
        if (isEquation()){
            Fraction[][] newMatrix = new Fraction[lastMatrix.length][lastMatrix[0].length-1];
            for (int i = 0; i < lastMatrix.length; i++) {
                boolean leftSide = true;
                for (int j = 0; j < lastMatrix[0].length; j++) {
                    if (lastMatrix[i][j] instanceof ParametricFraction && ((ParametricFraction) lastMatrix[i][j]).getParameterName().equals("=")){
                        leftSide = false;
                    }
                    else {
                        if (leftSide){
                            newMatrix[i][j] = new Fraction(lastMatrix[i][j]);
                        }else {
                            newMatrix[i][j-1] = new Fraction(lastMatrix[i][j]);
                        }
                    }
                }
            }
            addStep(new MatrixStep(newMatrix, "Vytvoříme rozšířenou matici dané soustavy rovnic."));
        }
    }

    private boolean isEquation(){
        for (Fraction[] row : lastMatrix) {
            for (Fraction fraction:row){
                if (fraction instanceof ParametricFraction) {
                    return true;
                }
            }
        }
        return false;
    }

    private void createParametricSolution(){
        ParametricSolver solver = new ParametricSolver(solution, eliminatedLinesCount);
        callAnotherSolver(solver);
        solvedVariables = solver.getSolvedVariables();
    }

    private void makeUnitSolution(){

        if (isHomogeneous()){
            createSolution(true);
            return;
        }
        for (int i = lastMatrix.length-1; i > 0; i--) {
            i-= removeAllIdenticalRows();
            if (!solvable){
                return;
            }
            for (int j = i-1; j >= 0; j--) {
                if (!solvable){
                    return;
                }
                for (int k = 0; k < lastMatrix[i].length- rightSideCount; k++) {
                    if (lastMatrix[i][k].getNumerator() != 0){
                        if (lastMatrix[j][k].getNumerator() != 0) {
                            if (createSteps(i, j, k))
                                i--;
                        }
                        if (lastMatrix[i][k].toDouble() != 1){
                            Fraction[][] matrix = MathFunctions.clone2DArray(lastMatrix);
                            MathFunctions.divideRow2DArray(matrix, i, lastMatrix[i][k]);
                            addStep(new MatrixStep(matrix, "Vydělíme "+ i + ". řádek tak, aby se první nenulové číslo rovnalo 1."));
                        }
                        if (!solvable) return;
                        break;
                    }
                }
            }
        }
        removeAllIdenticalRows();
        if (!regular){
            createParametricSolution();
        }else {
            createSolution(false);
        }
    }

    private boolean isHomogeneous(){
        if (rightSideCount !=1){
            return false;
        }
        for (Fraction[] row:lastMatrix) {
            if (row[row.length-1].getNumerator()!= 0){
                return false;
            }
        }
        return true;
    }

    private boolean checkZeroedRow(int row){
        Fraction[] fractions = lastMatrix[row];
        for (int i = 0; i < fractions.length - 1; i++) {
            Fraction f = fractions[i];
            if (f.getNumerator() != 0) {
                return true;
            }
        }
        if (fractions[fractions.length-1].getNumerator() != 0){
            solvable = false;
            Fraction[][] newMatrix = new Fraction[1][1];
            newMatrix[0][0] = new Fraction(0);
            addStep(new MatrixStep(newMatrix, "Hodnost matice je menší než hodnost rozšířené matice, takže tato soustava rovnic nemá řešení."));

        }else {
            Fraction[][] matrix = MathFunctions.clone2DArray(lastMatrix);
            MathFunctions.switchRows2DArray(matrix, row, matrix.length-1);
            Fraction[][] newMatrix = MathFunctions.clone2DArray(matrix, matrix.length-1, matrix[0].length, 0,0);
            addStep(new MatrixStep(newMatrix, "Vyškrtneme nulový řádek."));
            regular = false;
            eliminatedLinesCount++;

        }
        return false;
    }

    private void switchZeroFromPivot(int row){
        Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
        String switchRows = MathFunctions.switchZeroFromPivot(newMatrix, row);
        if (switchRows.length() >1){
            addStep(new MatrixStep(newMatrix, switchRows));
        }
    }

    private boolean createSteps(int controlRow, int nullableRow, int column){
        nullNumberInRow(controlRow, nullableRow, column);
        if (checkZeroedRow(nullableRow)){
            if (!solvable){
                return true;
            }
            divideRowSteps(nullableRow);
            return false;
        }
        return true;
    }

    private void removeIdenticalRow(int controlRow, int nullableRow){
        Fraction[][] matrix = MathFunctions.clone2DArray(lastMatrix);
        MathFunctions.subtractMatrixRows(matrix, controlRow, nullableRow);
        addStep(new MatrixStep(matrix, StringCreator.nullNumberInRowExplanation(nullableRow, controlRow, new Fraction(1), new Fraction(1))));
        checkZeroedRow(nullableRow);
        switchZeroFromPivot(controlRow);
    }

    private int removeAllIdenticalRows(){
        int counter = 0;
        int[] rows = new int[2];
        while(MathFunctions.findIdenticalRows(lastMatrix, rightSideCount, rows)){
            removeIdenticalRow(rows[0], rows[1]);
            rows = new int[2];
            counter++;
            if (!solvable) return counter;
        }
        return counter;
    }

    private void divideRowSteps(int row){
        int gcd =MathFunctions.divisorOfRow(lastMatrix, row);
        if (MathFunctions.changeSignumOfRow(lastMatrix, row)){
            gcd *=-1;
        }
        int nonZeroLeftSide = MathFunctions.countNonZeroFractions(lastMatrix[row], rightSideCount);
        if (Math.abs(gcd) !=1 || (nonZeroLeftSide ==1 && gcd != 1)){
            Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
            MathFunctions.divideRow2DArray(newMatrix, row,gcd);
            String explanation = "Vydělíme " + (row+1) + ". řádek číslem " + gcd + ".";
            addStep(new MatrixStep(newMatrix, explanation));
        }
    }

    private void nullNumberInRow(int controlRow, int nullableRow, int positionInRow){
        if (lastMatrix[controlRow][positionInRow].toDouble()!=0) {
            Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
            String explanation = MathFunctions.nullNumberInMatrix(newMatrix, controlRow, nullableRow, positionInRow);
            addStep(new MatrixStep(newMatrix, explanation));
        }
    }

    private void createSolution(boolean homogeneous){
        Fraction[][] solutionMatrix = new Fraction[lastMatrix.length][lastMatrix[0].length-lastMatrix.length+2];
        for (int i = 0; i < lastMatrix.length; i++) {
            solutionMatrix[i][0] = new ParametricFraction(1, VARIABLES[i]);
            solutionMatrix[i][1] = new ParametricFraction(1, "=");
            List<Fraction> list = new ArrayList<>();
            for (int j = lastMatrix.length; j < lastMatrix[i].length; j++) {
                list.add(lastMatrix[i][j]);
                solutionMatrix[i][j-lastMatrix.length+2] = lastMatrix[i][j];
            }
            solvedVariables.put(VARIABLES[i], list);
        }
        if (homogeneous){
            addStep(new MatrixStep(solutionMatrix, "Jelikož daná soustava rovnic je homogenní a hodnost matice soustavy = počtu proměnných, tak je řešení této soustavy rovnic nulové."));
        }else if (rightSideCount==1){
            addStep(new MatrixStep(solutionMatrix, "Vypíšeme řešení dané soustavy rovnic."));
        }
    }

}
