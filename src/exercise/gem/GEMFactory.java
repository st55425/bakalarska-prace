package exercise.gem;

import exercise.Fraction;
import exercise.IExercise;
import exercise.MatrixStep;
import exercise.ParametricFraction;
import exercise.utils.Generator;
import exercise.utils.MathFunctions;
import org.ejml.simple.SimpleMatrix;

import java.util.Random;

public final class GEMFactory {

    private GEMFactory(){}

    public static IExercise generateExercise(int numberOfRows, int max) {
        SimpleMatrix task;
        MatrixStep taskStep;
        Random random = new Random();
        boolean normalExercise = random.nextBoolean();
        boolean zeroExercise = random.nextBoolean();
        do {
            if (normalExercise){
                task = generateGoodExercise(numberOfRows, max);
            }else {
                Fraction[][] singularMatrix = MathFunctions.clone2DArray(Generator.generateSingularMatrix(numberOfRows+1, -max, max, 1), numberOfRows, numberOfRows+1, 0, 0);
                if (zeroExercise){
                    for (int i = 0; i < numberOfRows; i++) {
                        singularMatrix[i][numberOfRows] = new Fraction(random.nextInt(2*max)-max);
                    }
                }
                task = new SimpleMatrix(numberOfRows, numberOfRows+1);
                for (int i = 0; i < numberOfRows; i++) {
                    for (int j = 0; j < numberOfRows+1; j++) {
                        task.set(i,j,singularMatrix[i][j].getNumerator());
                    }
                }
            }

            taskStep = new MatrixStep(taskToEquations(task),
                    "Vypočítejte danou soustavu rovnic. V případě, že má daná soustava jediné řešení, použijte Gauss-Jordanovu metodu, jinak použijte Gaussovu eliminaci.");
        }while (new GEMSolver(taskStep, 1).getMaxAbsNumberInSolution() > Math.sqrt(Math.pow(max, numberOfRows)));

        return new GEMExercise(taskStep);
    }

    private static Fraction[][] taskToEquations(SimpleMatrix task){
        Fraction[][] matrix = new Fraction[task.numRows()][task.numCols()+1];
        for (int i = 0; i < task.numRows(); i++) {
            for (int j = 0; j < task.numCols(); j++) {
                if (j == task.numCols()-1){
                    matrix[i][j] = new ParametricFraction(1, "=");
                    matrix[i][j+1] = new Fraction(task.get(i,j));
                }else {
                    matrix[i][j] = new ParametricFraction(task.get(i,j), GEMSolver.VARIABLES[j]);
                }
            }
        }
        return matrix;
    }

    //nefunguje pro vetsi matice pri vetsich cislech - omezit v aplikaci
    public static SimpleMatrix generateGoodExercise(int numberOfRows, int max){
        double[] solutionVector = Generator.generateDoubleVector(numberOfRows, -max, max);
        double[][] taskDoubleMatrix = new double[numberOfRows][numberOfRows];
        boolean again;
        double[] taskDoubleVector = new double[numberOfRows];
        do {
            for (int i = 0; i < numberOfRows; i++) {
                again = true;
                while (again){
                    again = false;
                    taskDoubleMatrix[i] = Generator.generateDoubleVector(numberOfRows, -max, max);
                    taskDoubleVector[i] = 0;
                    for (int j = 0; j < taskDoubleMatrix[i].length; j++) {
                        double value = taskDoubleMatrix[i][j];
                        taskDoubleVector[i] += value * solutionVector[j];
                    }
                    if (Math.abs(taskDoubleVector[i])> max){
                        again = true;
                    }
                }
            }
        }while (new SimpleMatrix(taskDoubleMatrix).determinant() == 0);


        SimpleMatrix taskVector = new SimpleMatrix(numberOfRows, 1, true, taskDoubleVector);
        SimpleMatrix taskMatrix = new SimpleMatrix(taskDoubleMatrix);

        return taskMatrix.combine(0, numberOfRows, taskVector);
    }
}
