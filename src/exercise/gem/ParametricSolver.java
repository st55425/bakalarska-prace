package exercise.gem;

import exercise.Fraction;
import exercise.MatrixSolver;
import exercise.MatrixStep;
import exercise.ParametricFraction;
import exercise.utils.Generator;
import exercise.utils.MathFunctions;

import java.util.*;


public class ParametricSolver extends MatrixSolver {

    public static final String[] PARAMETERS = new String[]{"t", "s", "r", "p", "o"};

    private final int eliminatedLinesCount;
    private final HashMap<String, List<Fraction>> solvedVariables;

    public ParametricSolver(MatrixStep task, int eliminatedLinesCount) {
        super(task);
        this.eliminatedLinesCount = eliminatedLinesCount;
        solvedVariables = new HashMap<>();
        makeParametricSolution();

    }

    public HashMap<String, List<Fraction>> getSolvedVariables() {
        return solvedVariables;
    }

    private void makeParametricSolution(){
        rewriteToEquations();
        solveVars();
        for (int i = 0; i < eliminatedLinesCount; i++) {
            addParameter(i);
            solveVars();
        }
    }

    private void solveVars(){
        boolean changed = true;
        while (changed){
            changed = false;
            for (int i = 0; i < lastMatrix.length; i++) {
                Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
                int varCount = 0;
                String variable = "";
                int position=0;
                for (int j = 0; j < newMatrix[0].length; j++) {
                    if (newMatrix[i][j].getNumerator() != 0
                            && newMatrix[i][j] instanceof ParametricFraction
                            && Arrays.asList(GEMSolver.VARIABLES).contains(((ParametricFraction)newMatrix[i][j]).getParameterName())){
                        varCount++;
                        position = j;
                        variable = ((ParametricFraction)newMatrix[i][j]).getParameterName();
                    }
                }
                List<Fraction> solution = new ArrayList<>();
                if (varCount ==1 && !solvedVariables.containsKey(variable)){
                    if (newMatrix[i][position].toDouble() != 1) {
                        newMatrix = MathFunctions.clone2DArray(lastMatrix);
                        MathFunctions.divideRow2DArray(newMatrix, i, newMatrix[i][position].toDouble());
                        addStep(new MatrixStep(newMatrix, "Vyřešíme proměnnou " + variable + "."));
                    }
                    for (int j = 0; j < newMatrix[0].length; j++) {
                        if (newMatrix[i][j].getNumerator() != 0){
                            if (newMatrix[i][j] instanceof ParametricFraction){
                                if (!Arrays.asList(GEMSolver.VARIABLES).contains(((ParametricFraction)newMatrix[i][j]).getParameterName())
                                        && !((ParametricFraction)newMatrix[i][j]).getParameterName().equals("=")){
                                    solution.add(newMatrix[i][j]);
                                }
                            }else {
                                solution.add(newMatrix[i][j]);
                            }
                        }
                    }
                    if (solution.size() ==0){
                        solution.add(new Fraction(0));
                    }
                    solvedVariables.put(variable, solution);
                    substituteVariable(variable);
                    changed = true;
                }
            }
        }
    }

    private void addParameter(int parameterCount){
        Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
        int row = 0;
        String variable = "";
        for (int i = newMatrix.length-1; i >= 0; i--) {
            if (!solvedVariables.containsKey(GEMSolver.VARIABLES[i])){
                variable = GEMSolver.VARIABLES[i];
                row = i;
                break;
            }
        }

        newMatrix[row] = Generator.generateFractionArray(newMatrix[0].length, 0);
        newMatrix[row][0] = new ParametricFraction(1,variable);
        newMatrix[row][1] = new ParametricFraction(1,"=");
        newMatrix[row][2] = new ParametricFraction(1,PARAMETERS[parameterCount]);
        addStep(new MatrixStep(newMatrix, "Vyjádříme proměnnou " + variable +" jako parametr " + PARAMETERS[parameterCount] + "."));
        List<Fraction> solution = new ArrayList<>();
        solution.add(newMatrix[row][2]);
        solvedVariables.put(variable, solution);
        substituteVariable(variable);
    }

    private void rewriteToEquations(){
        int numberOfVariables = lastMatrix[0].length-1;
        Fraction[][] newMatrix = Generator.generateFractionMatrix(numberOfVariables, numberOfVariables+2+eliminatedLinesCount, 0);
        for (int i = 0; i < lastMatrix.length; i++) {
            boolean firstNotZero = true;
            int row = i;
            for (int j = 0; j < newMatrix[0].length; j++) {
                if (j < numberOfVariables) {
                    if (lastMatrix[i][j].getNumerator() != 0) {
                        if (firstNotZero) {
                            firstNotZero = false;
                            row = j;
                        }
                        newMatrix[row][j] = new ParametricFraction(lastMatrix[i][j], GEMSolver.VARIABLES[j], true, 1);
                    }
                }else if (j == numberOfVariables){
                        newMatrix[row][j] = new ParametricFraction(1, "=");
                }
                else if (j-1 < lastMatrix[0].length){
                    newMatrix[row][j] = new Fraction(lastMatrix[i][j-1]);
                }
            }
        }


        addStep(new MatrixStep(newMatrix, "Přepíšeme matici do rovnic."));
    }

    private void substituteVariable(String variable){

        if (!solvedVariables.containsKey(variable)){
            throw new IllegalArgumentException("Pokus o substituci nevyřešené proměnné.");
        }

        Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
        int offset = solvedVariables.get(variable).size()-1;
        boolean substituted = false;

        for (int i = 0; i <newMatrix.length; i++) {
            double multiple = 0;
            int varIndex = 0;
            int varCount = 0;
            for (int j = 0; j < newMatrix[0].length; j++) {
                if (newMatrix[i][j] instanceof ParametricFraction
                        && Arrays.asList(GEMSolver.VARIABLES).contains(((ParametricFraction)newMatrix[i][j]).getParameterName())
                        && newMatrix[i][j].getNumerator() != 0){
                    varCount++;
                    if (((ParametricFraction)newMatrix[i][j]).getParameterName().equals(variable)){
                        multiple = newMatrix[i][j].toDouble();
                        varIndex = j;
                    }
                }

            }
            if (multiple != 0 && varCount >1){
                if (offset != 0){
                    for (int j = newMatrix[0].length-eliminatedLinesCount-offset; j >= varIndex ; j--) {
                        newMatrix[i][j+offset] = MathFunctions.copyFraction(newMatrix[i][j]);
                    }
                }
                for (int j = 0; j <= offset; j++) {
                    newMatrix[i][varIndex+j] = solvedVariables.get(variable).get(j).multiply(multiple);
                }
                substituted = true;
            }

        }
        if (substituted){
            addStep(new MatrixStep(newMatrix, "Dosadíme nově vypočítaný parametr do ostatních rovnic."));
            if (solvedVariables.get(variable).size() > 1 || !solvedVariables.get(variable).get(0).equals(new Fraction(0))){
                reorganizeParametricMatrix();
            }
        }
    }

    private void reorganizeParametricMatrix(){
        Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
        for (int i = 0; i < newMatrix.length; i++) {
            newMatrix[i] = reorganizeParametricRow(newMatrix[i]);
        }
        addStep(new MatrixStep(newMatrix, "Přesuneme parametry a čísla na pravou stranu rovnice."));
    }

    private Fraction[] reorganizeParametricRow(Fraction[] row){
        Fraction[] newRow = Generator.generateFractionArray(row.length, 0);
        List<Fraction> variables = new ArrayList<>();
        LinkedHashMap<String, Fraction> values = new LinkedHashMap<>();
        boolean rightSide = false;
        for (int i = 0; i < row.length; i++) {
            if (row[i].getNumerator() !=0){
                if (row[i] instanceof ParametricFraction ){
                    ParametricFraction tmp = (ParametricFraction)row[i];
                    if (Arrays.asList(GEMSolver.VARIABLES).contains(tmp.getParameterName())){
                        variables.add(MathFunctions.copyFraction(row[i]));
                    }else if (tmp.getParameterName().equals("=")){
                        rightSide = true;
                    }else if (rightSide){
                        if (values.containsKey(tmp.getParameterName())){
                            values.put(tmp.getParameterName(), values.get(tmp.getParameterName()).add(row[i]));
                        }else{
                            values.put(tmp.getParameterName(), MathFunctions.copyFraction(row[i]));
                        }
                    }else {
                        if (values.containsKey(tmp.getParameterName())){
                            values.put(tmp.getParameterName(), values.get(tmp.getParameterName()).subtract(row[i]));
                        }else{
                            values.put(tmp.getParameterName(), row[i].opposite());
                        }
                    }
                }
                else if (rightSide){
                    if (values.containsKey("number")){
                        values.put("number", values.get("number").add(row[i]));
                    }else{
                        values.put("number", MathFunctions.copyFraction(row[i]));
                    }
                }
                else {
                    if (values.containsKey("number")){
                        values.put("number", values.get("number").subtract(row[i]));
                    }else{
                        values.put("number", row[i].opposite());
                    }
                }
            }
        }

        for (int i = 0; i < variables.size(); i++) {
            newRow[i] = variables.get(i);
        }
        if (variables.size() >0){
            newRow[variables.size()] = new ParametricFraction(1,"=");
        }

        int index = variables.size()+1;
        for (Fraction value:values.values()) {
            newRow[index] = value;
            index++;
        }

        return newRow;
    }
}
