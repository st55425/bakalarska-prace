package exercise.gem;

import exercise.*;

public class GEMExercise extends MatrixExercise {

    public GEMExercise(MatrixStep task ) {
        super(task);
        solver = new GEMSolver(task, 1);
    }

    @Override
    public Type getType() {
        return Type.GEM;
    }
}
