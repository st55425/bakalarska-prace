package exercise.gem.inversion;

import exercise.IExercise;
import exercise.MatrixStep;
import exercise.utils.Generator;
import org.ejml.simple.SimpleMatrix;

public final class InversionFactory {

    private InversionFactory(){}

    public static IExercise generateExercise(int numberOfRows, int max) {
        SimpleMatrix taskMatrix;
        MatrixStep taskStep;
        do {
            taskMatrix =  Generator.generateUnimodularMatrix(numberOfRows, -max, max);
            taskStep = new MatrixStep(taskMatrix.getDDRM().data, numberOfRows,
                    numberOfRows, "Vypočítejte inverzní matici.");
        }while (new InversionSolver(taskStep).getMaxAbsNumberInSolution() > Math.sqrt(Math.pow(max, numberOfRows)));
        return new InversionExercise(taskStep);
    }
}
