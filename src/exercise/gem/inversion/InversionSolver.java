package exercise.gem.inversion;

import exercise.Fraction;
import exercise.MatrixSolver;
import exercise.gem.GEMSolver;
import exercise.MatrixStep;
import exercise.utils.MathFunctions;
import org.ejml.simple.SimpleMatrix;

public class InversionSolver extends MatrixSolver {

     InversionSolver(MatrixStep task) {
        super(task);
        makeSolution();
    }


    private void makeSolution(){
         createExtendedMatrix();
         GEMSolver solver = new GEMSolver(solution, lastMatrix.length);
         callAnotherSolver(solver);

         if (solver.isRegular()){
             Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix,lastMatrix.length, lastMatrix.length, 0,lastMatrix.length);
             addStep(new MatrixStep(newMatrix, "Na pravé straně nám vznikla inverzní matice k původní matici. Pro přehlednost odstraníme jednotkovou matici na levé straně."));
         }else {
             Fraction[] zero = new Fraction[]{new Fraction(0)};
             addStep(new MatrixStep(zero,1,1, "k zadané matici neexistuje inverzní matice."));
         }
    }

    private void createExtendedMatrix(){
        SimpleMatrix identityMatrix = SimpleMatrix.identity(lastMatrix.length);
        SimpleMatrix taskMatrix = new SimpleMatrix(MathFunctions.clone2DArrayToDouble(lastMatrix));
        SimpleMatrix nextStepMatrix = taskMatrix.combine(0, lastMatrix.length, identityMatrix);
        MatrixStep nextStep =  new MatrixStep(nextStepMatrix.getDDRM().data, lastMatrix.length, lastMatrix.length*2,
                "Vytvoříme rozšířenou matici tak, že na pravou stranu připíšeme jednotkovou matici. Pomocí " +
                        "řádkových elementárních úprav převedeme jednotkovou matici na levou stranu.");

        addStep(nextStep);
    }

}
