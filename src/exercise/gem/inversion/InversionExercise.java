package exercise.gem.inversion;

import exercise.*;

public class InversionExercise extends MatrixExercise {

    public InversionExercise(MatrixStep task) {
        super(task);
        solver = new InversionSolver(task);
    }

    @Override
    public Type getType() {
        return Type.INVERSION;
    }
}
