package exercise;

import java.util.List;
import java.util.NoSuchElementException;

public class SolutionIterator implements ISolutionIterator {
    protected List<IStep> steps;
    protected int index;


    public SolutionIterator(List<IStep> steps){
        this.steps = steps;
        index = -1;
    }


    @Override
    public boolean hasNextStep() {
        return index<steps.size()-1;
    }

    @Override
    public boolean hasPreviousStep() {
        return index >0;
    }

    @Override
    public IStep nextStep() {
        if(!hasNextStep()){
            throw new NoSuchElementException();
        }
        index++;
        return steps.get(index);
    }

    @Override
    public IStep previousStep() {
        if(!hasPreviousStep()){
            throw new NoSuchElementException();
        }
        index--;
        return steps.get(index);
    }

}
