package exercise;

import exercise.utils.MathFunctions;

import java.util.ArrayList;
import java.util.List;

public abstract class MatrixSolver implements ISolver{

    protected final List<MatrixStep> steps;
    protected Fraction[][] lastMatrix;
    protected MatrixStep solution;

    @Override
    public List<IStep> getFullSolution(){
        return new ArrayList<>(steps);
    }

    @Override
    public MatrixStep getSolution(){
        return solution;
    }

    public MatrixSolver(MatrixStep task) {
        steps = new ArrayList<>();
        lastMatrix = MathFunctions.clone2DArray(task.getMatrix());
        solution = task;
        steps.add(task);
    }

    public double getMaxAbsNumberInSolution(){
        double max = 0;
        for (MatrixStep step:steps) {
            double maxInStep = MathFunctions.findAbsMaximumNumberInMatrix(step.matrix);
            if (maxInStep > max){
                max = maxInStep;
            }
        }
        return max;
    }

    protected void addStep(MatrixStep step){
        steps.add(step);
        solution = step;
        lastMatrix = step.getMatrix();
    }

    protected void callAnotherSolver(MatrixSolver solver){
        steps.remove(solution);
        steps.addAll(solver.steps);
        solution = solver.solution;
        lastMatrix = solution.getMatrix();
    }
}
