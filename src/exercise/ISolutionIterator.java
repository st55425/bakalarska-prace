package exercise;

public interface ISolutionIterator {

    boolean hasNextStep();

    boolean hasPreviousStep();

    IStep nextStep();

    IStep previousStep();

}
