package exercise;

public enum Type {
    GEM("Soustavy rovnic"),
    DETERMINANT("Determinant"),
    INVERSION("Inverzní matice"),
    SARRUS("Sarrusovo pravidlo"),
    EIGEN("Vlastní čísla a vektory"),
    COMBINATION("Lineární kombinace vektorů"),
    DEPENDENCY("Lineární závislost vektorů");



    private final String name;

    Type(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
