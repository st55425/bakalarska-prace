package exercise;

import exercise.utils.MathFunctions;
import exercise.utils.StringCreator;

public class MatrixStep implements IStep {

    protected Fraction[][] matrix;
    protected String explanation;


    public MatrixStep(Fraction[][] matrix, String explanation) {
        this.matrix = matrix;
        this.explanation = explanation;
    }

    public MatrixStep(double[][] matrix, String explanation) {
        this.matrix = MathFunctions.clone2DArray(matrix);
        this.explanation = explanation;
    }

    public MatrixStep(double[] matrix, int numberOfRows, int numberOfColumns, String explanation) {
        this.matrix = MathFunctions.matrixFromArray(matrix, numberOfRows, numberOfColumns);
        this.explanation = explanation;
    }

    public MatrixStep(Fraction[] matrix, int numberOfRows, int numberOfColumns, String explanation) {
        this.matrix = MathFunctions.matrixFromArray(matrix, numberOfRows, numberOfColumns);
        this.explanation = explanation;
    }

    @Override
    public String getData() {
        return StringCreator.matrixToString(matrix);
    }

    @Override
    public String getExplanation() {
        return explanation;
    }

    @Override
    public int getSize() {
        int size = 0;
        int zeroLinesCount = 0;
        boolean allNonParametric = true;
        for (int i = 0; i < matrix.length; i++) {
            boolean allZeros = true;
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j]instanceof ParametricFraction){
                    allNonParametric = false;
                }
                if (matrix[i][j].getNumerator() != 0){
                    allZeros = false;
                }
                if (!matrix[i][j].isInteger()){
                    size++;
                    break;
                }
            }
            if (allZeros){
                zeroLinesCount++;
            }
            if (!allZeros || zeroLinesCount <1){
                size++;
            }
        }
        if (allNonParametric){
            return matrix.length;
        }
        return size;
    }

    public Fraction[][] getMatrix() {
        return matrix;
    }
}


