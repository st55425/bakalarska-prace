package exercise.sarrus;

import exercise.Fraction;
import exercise.MatrixSolver;
import exercise.MatrixStep;
import exercise.ParametricFraction;
import exercise.utils.Generator;
import exercise.utils.MathFunctions;


public class SarrusSolver extends MatrixSolver {

    private Fraction[][] extendedMatrix;
    private static final int NUMBER_OF_COLUMNS = 3;

    public SarrusSolver(MatrixStep task) {
        super(task);
        makeSolution();
    }



    private void makeSolution(){
        createExtendedMatrix();
        Fraction solution = new Fraction(0);
        Fraction[] diagonal;
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            diagonal = getMainDiagonal(i);
            solution = computeIntermediateSolution(solution, diagonal, i);
        }
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            diagonal = getSecondaryDiagonal(i);
            solution = computeIntermediateSolution(solution, diagonal, i+3);
        }
        finalSolution();
    }

    private void createExtendedMatrix(){

        Fraction[][] newMatrix = new Fraction[NUMBER_OF_COLUMNS+2][NUMBER_OF_COLUMNS];
        for (int i = 0; i < NUMBER_OF_COLUMNS+2; i++) {
            newMatrix[i] = MathFunctions.cloneArray(lastMatrix[i%lastMatrix.length]);
        }
        extendedMatrix = newMatrix;
        addStep(new MatrixStep(newMatrix, "Pod původní matici přepíšeme první dva řádky."));
    }

    private Fraction[] getMainDiagonal(int startRow){
        Fraction[] diagonal = Generator.generateFractionArray(3, 0);
        if (startRow==0){
            addStep(new MatrixStep(extendedMatrix, "Vezmeme prvky na hlavní diagonále."));
        }else{
            addStep(new MatrixStep(extendedMatrix, "Vezmeme prvky na diagonále o " + startRow + " pod hlavní diagonálou."));
        }
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            diagonal[i] = new Fraction(extendedMatrix[i+startRow][i]);
        }
        return diagonal;
    }

    private Fraction[] getSecondaryDiagonal(int startRow){
        Fraction[] diagonal = Generator.generateFractionArray(3, 0);
        for (int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            diagonal[i] = new Fraction(extendedMatrix[i+startRow][NUMBER_OF_COLUMNS -i -1]);
        }
        if (startRow == 0){
            addStep(new MatrixStep(extendedMatrix, "Vezmeme prvky na vedlejší diagonále."));
        }else{
            addStep(new MatrixStep(extendedMatrix, "Vezmeme prvky na diagonále o " + startRow + " pod vedlejší diagonálou."));
        }
        return diagonal;
    }

    private Fraction computeIntermediateSolution(Fraction solution, Fraction[] diagonal, int row){
        if (diagonal.length != 3){
            throw new IllegalArgumentException("Diagonála má špatný rozměr.");
        }
        Fraction[] extendedSolution = new Fraction[6];
        extendedSolution[0] = solution;
        Fraction intermediateStep = new Fraction(1);
        for (int i = 0; i < diagonal.length; i++) {
            extendedSolution[i+1] = diagonal[i];
            intermediateStep = intermediateStep.multiply(diagonal[i]);
        }
        extendedSolution[4] = new ParametricFraction(1,"=");
        if (row ==0){
            extendedSolution[5] = intermediateStep;
            addStep(new MatrixStep(extendedSolution, 1,6, "Tyto prvky mezi sebou vynásobíme a později k nim budeme přidávát další mezivýsledky."));
        }
        else if (row <3){
            extendedSolution[5] = solution.add(intermediateStep);
            addStep(new MatrixStep(extendedSolution, 1,6, "Tyto prvky mezi sebou vynásobíme a přičteme je k předchozím mezivýsledkům."));
        }
        else {
            extendedSolution[5] = solution.subtract(intermediateStep);
            addStep(new MatrixStep(extendedSolution,1,6, "Tyto prvky mezi sebou vynásobíme a odečteme je od předchozích mezivýsledků."));
        }
        return extendedSolution[5];
    }

    private void finalSolution(){
        Fraction[] finalSolution = new Fraction[1];
        finalSolution[0] = lastMatrix[0][5];
        addStep(new MatrixStep(finalSolution,1, 1, "Vypíšeme výsledek."));
    }

}
