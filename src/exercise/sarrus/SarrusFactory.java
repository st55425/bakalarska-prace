package exercise.sarrus;

import exercise.IExercise;
import exercise.MatrixStep;
import exercise.utils.Generator;
import org.ejml.simple.SimpleMatrix;

public final class SarrusFactory {

    private SarrusFactory(){}

    public static IExercise generateExercise(int max){
        int numberOfRows = 3;
        SimpleMatrix taskMatrix;
        do {
            taskMatrix = Generator.generateRegularMatrix(numberOfRows, -max, max);
        }while (Math.abs(taskMatrix.determinant()) > Math.pow(max, numberOfRows)/(numberOfRows *max));

        MatrixStep taskStep = new MatrixStep(taskMatrix.getDDRM().data, 3, 3,
                "Vypočítejte zadaný determinant pomocí Sarrusova pravidla.");
        return new SarrusExercise(taskStep);
    }
}
