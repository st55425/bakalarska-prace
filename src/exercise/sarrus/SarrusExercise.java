package exercise.sarrus;

import exercise.*;

public class SarrusExercise extends MatrixExercise {

    public SarrusExercise(MatrixStep task) {
        super(task);
        solver = new SarrusSolver(task);
    }

    @Override
    public Type getType() {
        return Type.SARRUS;
    }
}
