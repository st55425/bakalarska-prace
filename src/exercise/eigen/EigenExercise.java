package exercise.eigen;

import exercise.*;

import java.util.List;

public class EigenExercise extends MatrixExercise {


    public EigenExercise(MatrixStep task, List<Double> eigenValues) {
        super(task);
        solver = new EigenSolver(task, eigenValues);
    }

    @Override
    public Type getType() {
        return Type.EIGEN;
    }

    @Override
    public int getSize() {
        if (getSolution().getData().contains("x_2")){
            String[] splited = getSolution().getData().split("x_2");
            String vector = splited[1].substring(0, task.getSize()*2);
            if (vector.contains("/")){
                return Math.max(task.getSize(), getSolution().getSize()+2);
            }
            return Math.max(task.getSize(), getSolution().getSize()+1);
        }
        return Math.max(task.getSize(), getSolution().getSize());
    }
}
