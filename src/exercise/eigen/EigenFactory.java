package exercise.eigen;

import exercise.IExercise;
import exercise.MatrixStep;
import exercise.utils.Generator;
import org.ejml.simple.SimpleMatrix;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public final class EigenFactory {

    private EigenFactory(){}

    //Funguje max pro velikost 4
    public static IExercise generateExercise(int matrixSize, int max){
        Random random = new Random();
        double[] eigenValues = new double[matrixSize];
        double eigenValuesMultiple;
        SimpleMatrix result;
        do {
            SimpleMatrix randomMatrix;
            do {
                randomMatrix = Generator.generateRegularMatrix(matrixSize, -5, 5);
            }while (Math.abs(randomMatrix.determinant()) > 5);
            double determinant = randomMatrix.determinant();

            eigenValuesMultiple = 1;
            int multiple = random.nextInt((int) Math.abs(determinant));
            for (int i = 0; i < matrixSize; i++) {
                do {
                    eigenValues[i] = random.nextInt(2*max)-max;
                }while (eigenValues[i]%determinant != multiple);
                eigenValuesMultiple *= eigenValues[i];
            }
            SimpleMatrix diagonal = SimpleMatrix.diag(eigenValues);
            SimpleMatrix inverse = randomMatrix.invert();
            result = randomMatrix.mult(diagonal);
            result = result.mult(inverse);
        }while (result.elementMaxAbs() > max || !duplicate(eigenValues, matrixSize-2) || eigenValuesMultiple > Math.max(20, Math.pow(matrixSize, matrixSize)));

        MatrixStep task = new MatrixStep(result.getDDRM().data, matrixSize, matrixSize, "Určete vlastní čísla a vlastní vektory zadané matice.");
        return new EigenExercise(task, Arrays.stream(eigenValues).boxed().collect(Collectors.toList()));
    }

    private static boolean duplicate(double[] array, int tolerance){
        int fault = 0;
        Set<Double> set = new HashSet<>();
        for (double number: array) {
            if (!set.add(number)){
                fault++;
                if (fault > tolerance){
                    return false;
                }
            }
        }
        return true;
    }

}
