package exercise.eigen;

import exercise.Fraction;
import exercise.MatrixSolver;
import exercise.MatrixStep;
import exercise.ParametricFraction;
import exercise.gem.GEMSolver;
import exercise.gem.ParametricSolver;
import exercise.utils.Generator;
import exercise.utils.MathFunctions;

import java.util.*;

public class EigenSolver extends MatrixSolver {

    private final List<Double> eigenValues;
    private Double[] nonDuplicateEigenNumbers;
    private final List<HashMap<String, List<Fraction>>> eigenVectors;
    private final Fraction[][] task;

    public EigenSolver(MatrixStep task, List<Double> eigenValues) {
        super(task);
        this.task = task.getMatrix();
        this.eigenValues = eigenValues;
        eigenVectors = new ArrayList<>();
        Set<Double> set = new HashSet<>(eigenValues);
        nonDuplicateEigenNumbers = new Double[1];
        nonDuplicateEigenNumbers = set.toArray(nonDuplicateEigenNumbers);
        makeSolution();
    }


    private void makeSolution() {
        addLambdas();
        if (createPolynom()) {
            rewriteToPolynoms();
        } else {
            polynomFromSolution();
        }
        createCharacteristicEquation();
        solveEquation();
        for (Double eigenValue : nonDuplicateEigenNumbers) {
            findVector(eigenValue);
        }
        finishSolution();
    }

    private void addLambdas() {
        Fraction[][] matrix = MathFunctions.clone2DArray(lastMatrix);
        for (int i = 0; i < lastMatrix.length; i++) {
            matrix[i][i] = new ParametricFraction(matrix[i][i].toDouble(), "-λ", true);
        }
        addStep(new MatrixStep(matrix, "Vytvoříme charakteristickou matici původní matice.To uděláme tak, že od prvků na hlavní diagonále odečteme proměnnou λ. Vypočtením determinantu této matice vytvoříme charakteristický polynom."));
    }

    private boolean createPolynom() {
        Fraction[][] matrix;
        if (lastMatrix.length == 2) {
            matrix = new Fraction[2][2];
            matrix[0][0] = lastMatrix[0][0];
            matrix[0][1] = lastMatrix[1][1];
            matrix[1][0] = lastMatrix[0][1];
            matrix[1][1] = lastMatrix[1][0];
        } else if (lastMatrix.length == 3) {
            matrix = new Fraction[6][3];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (lastMatrix[(i + j) % 3][j].getNumerator()!=0 || lastMatrix[(i + j) % 3][j] instanceof ParametricFraction){
                        matrix[i][j] = MathFunctions.copyFraction(lastMatrix[(i + j) % 3][j]);
                    }
                    else {
                        matrix[i] = Generator.generateFractionArray(3,0);
                        break;
                    }
                }
            }
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (lastMatrix[(i + j) % 3][2 - j].getNumerator()!=0 || lastMatrix[(i + j) % 3][2 - j] instanceof ParametricFraction){
                        matrix[3 + i][j] = MathFunctions.copyFraction(lastMatrix[(i + j) % 3][2 - j]);
                    }
                    else {
                        matrix[3+i] = Generator.generateFractionArray(3,0);
                        break;
                    }
                }
            }
        } else {
            return false;
        }
        addStep(new MatrixStep(matrix, "Vypočítáme determinant např. pomocí Sarrusovy metody."));
        return true;
    }

    private void rewriteToPolynoms() {
        Fraction[][] matrix = Generator.generateFractionMatrix(lastMatrix.length, lastMatrix[0].length + 3, 1);
        for (int i = 0; i < lastMatrix.length; i++) {
            int offset = 0;
            for (int j = 0; j < lastMatrix[0].length; j++) {
                if (lastMatrix[i][j] instanceof ParametricFraction && ((ParametricFraction) lastMatrix[i][j]).getParameterName().equals("-λ")) {
                    matrix[i][j + offset++] = new Fraction(lastMatrix[i][j].getNumerator(), lastMatrix[i][j].getDenominator());
                    matrix[i][j + offset] = new ParametricFraction(-1, "λ");
                } else {
                    matrix[i][j + offset] = MathFunctions.copyFraction(lastMatrix[i][j]);
                }

            }
        }
        lastMatrix = matrix;
    }

    private void polynomFromSolution() {
        Fraction[][] matrix = new Fraction[eigenValues.size()][2];
        for (int i = 0; i < eigenValues.size(); i++) {
            matrix[i][0] = new ParametricFraction(1, "λ");
            matrix[i][1] = new Fraction(eigenValues.get(i) * -1);
        }
        lastMatrix = matrix;
    }

    private void createCharacteristicEquation() {
        List<Fraction> characteristicEquation = new ArrayList<>();
        if (lastMatrix[0].length < 3) {
            characteristicEquation = characteristicFromSolution();
        }
        for (int i = 0; i < lastMatrix.length; i++) {
            List<Fraction> row = new ArrayList<>();
            for (int j = 0; j < lastMatrix[i].length; j++) {
                List<Fraction> bracket = new ArrayList<>();
                bracket.add(lastMatrix[i][j]);
                if (lastMatrix[i].length > j + 1 && lastMatrix[i][j + 1] instanceof ParametricFraction) {
                    bracket.add(lastMatrix[i][j + 1]);
                    j++;
                }
                if (row.isEmpty()) {
                    row.addAll(bracket);
                } else {
                    row = multiplyCross(row, bracket);
                }
            }

            if (i >= lastMatrix.length/2){
                for (int j = 0; j < row.size(); j++) {
                    row.set(j, row.get(j).opposite());
                }
            }
            characteristicEquation.addAll(row);
            characteristicEquation = reduceList(characteristicEquation);
            row.clear();
        }
        Fraction[] equation = new Fraction[characteristicEquation.size() + 2];
        Collections.reverse(characteristicEquation);
        if (characteristicEquation.get(0).getNumerator() < 0) {
            for (int i = 0; i < characteristicEquation.size(); i++) {
                equation[i] = characteristicEquation.get(i).opposite();
            }
        } else {
            equation = characteristicEquation.toArray(equation);
        }
        equation[equation.length - 2] = new ParametricFraction(1, "=");
        equation[equation.length - 1] = new Fraction(0);
        addStep(new MatrixStep(equation, 1, equation.length, "Vytvoříme charakteristický polynom. Nyní vypočteme jeho kořeny."));
    }

    private List<Fraction> characteristicFromSolution() {
        List<Fraction> result = new ArrayList<>(Arrays.asList(lastMatrix[0]));
        for (int i = 1; i < lastMatrix.length; i++) {
            result = multiplyCross(result, Arrays.asList(lastMatrix[i]));
        }
        Collections.reverse(result);
        return result;
    }

    private List<Fraction> multiplyCross(List<Fraction> first, List<Fraction> second) {
        List<Fraction> result = new ArrayList<>();
        for (Fraction frac : first) {
            for (Fraction sec : second) {
                result.add(frac.multiply(sec));
            }
        }
        return reduceList(result);
    }


    private List<Fraction> reduceList(List<Fraction> list) {
        List<Fraction> reduced = new ArrayList<>();
        for (Fraction fraction : list) {
            boolean add = true;
            for (int i = 0; i < reduced.size(); i++) {
                if (fraction instanceof ParametricFraction && reduced.get(i) instanceof ParametricFraction) {
                    if (((ParametricFraction) fraction).getParameterName().equals(((ParametricFraction) reduced.get(i)).getParameterName()) &&
                            ((ParametricFraction) fraction).getExponent() == ((ParametricFraction) reduced.get(i)).getExponent() &&
                            ((ParametricFraction) fraction).isParameterInNumerator() == ((ParametricFraction) reduced.get(i)).isParameterInNumerator()) {
                        reduced.set(i, reduced.get(i).add(fraction));
                        add = false;
                        break;
                    }
                } else if (fraction instanceof ParametricFraction || reduced.get(i) instanceof ParametricFraction) {
                    add = true;
                } else {
                    reduced.set(i, reduced.get(i).add(fraction));
                    add = false;
                    break;
                }
            }
            if (add) {
                reduced.add(fraction);
            }
        }
        return reduced;
    }

    private void solveEquation() {
        Fraction[][] matrix = new Fraction[nonDuplicateEigenNumbers.length][3];
        for (int i = 0; i < nonDuplicateEigenNumbers.length; i++) {
            matrix[i][0] = new ParametricFraction(1, "λ_" + i);
            matrix[i][1] = new ParametricFraction(1, "=");
            matrix[i][2] = new Fraction(nonDuplicateEigenNumbers[i]);
        }
        addStep(new MatrixStep(matrix, "Vypočítáme kořeny charakteristického polynomu. Ty jsou zároveň vlastními čísly matiice."));
    }

    private void findVector(double eigenNumber) {
        Fraction[][] matrix = MathFunctions.clone2DArray(task, task.length, task.length +1, 0, 0 );
        for (int i = 0; i < matrix.length; i++) {
            matrix[i][i] = matrix[i][i].subtract(new Fraction(eigenNumber));
        }
        addStep(new MatrixStep(matrix, "Do charakteristické matice dosadíme za proměnnou λ vlastní číslo "+ (int)eigenNumber +
                ". Vypočtením následující soustavy rovnic získáme příslušné vlastní vektory."));

        GEMSolver solver = new GEMSolver(getSolution(), 1);
        callAnotherSolver(solver);
        eigenVectors.add(solver.getSolvedVariables());

        List<Fraction> vectors = writeEigenVectors(solver.getSolvedVariables());
        int rows = Collections.frequency(vectors, new ParametricFraction(1, ";"))+1;
        int columns = vectors.indexOf(new ParametricFraction(1, ";"));
        if (columns==-1){
            columns = vectors.size();
        }
        matrix = new Fraction[1][1];
        vectors.removeIf(new ParametricFraction(1, ";")::equals);
        matrix = MathFunctions.matrixFromArray(vectors.toArray(matrix[0]), rows, columns);

        addStep(new MatrixStep(matrix, "Vypíšeme vektory."));
    }

    private List<Fraction> writeEigenVectors(HashMap<String, List<Fraction>> matrixSolution){
        List<Fraction> solution = new ArrayList<>();
        Set<String> parameters = new HashSet<>();
        for (int i = 0; i < matrixSolution.size(); i++) {
            for (Fraction fr : matrixSolution.get(GEMSolver.VARIABLES[i])) {
                if (fr instanceof ParametricFraction){
                    parameters.add(((ParametricFraction) fr).getParameterName());
                }
            }
        }
        for (int i = 0; i < parameters.size(); i++) {
            solution.add(new ParametricFraction(1, "x_" + i));
            solution.add(new ParametricFraction(1, "="));
            solution.add(new ParametricFraction(1, ParametricSolver.PARAMETERS[i]));
            for (int j = 0; j < matrixSolution.size(); j++) {
                boolean zero = true;
                for (Fraction frac: matrixSolution.get(GEMSolver.VARIABLES[j])) {
                    if (frac instanceof ParametricFraction && ((ParametricFraction) frac).getParameterName().equals(ParametricSolver.PARAMETERS[i])){
                        solution.add(new Fraction(frac));
                        zero = false;
                    }
                }
                if (zero){
                    solution.add(new Fraction(0));
                }
            }
            if (i+1 < parameters.size()){
                solution.add(new ParametricFraction(1, ";"));
            }
        }
        return solution;
    }

    private void finishSolution(){
        Fraction[][] matrix = new Fraction[nonDuplicateEigenNumbers.length][1];
        for (int i = 0; i < matrix.length; i++) {
            List<Fraction> solutionList = new ArrayList<>();
            solutionList.add(new ParametricFraction(1, "λ_" + i));
            solutionList.add(new ParametricFraction(1, "="));
            solutionList.add(new Fraction(nonDuplicateEigenNumbers[i]));
            solutionList.add(new ParametricFraction(1, ";"));
            solutionList.addAll(writeEigenVectors(eigenVectors.get(i)));
            matrix[i] = solutionList.toArray(matrix[i]);
        }
        addStep(new MatrixStep(matrix, "Vypíšeme výsledky."));
    }
}
