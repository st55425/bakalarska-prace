package exercise.determinant;

import exercise.Fraction;
import exercise.MatrixStep;
import exercise.utils.StringCreator;

public class DeterminantStep extends MatrixStep {

    private static final String defaultExplanation = "Vypočítejte zadaný determinant.";

    private final Fraction multiple;

    DeterminantStep(Fraction[][] matrix, String explanation, Fraction multiple) {
        super(matrix, explanation);
        this.multiple = multiple;
    }

    DeterminantStep(double[] matrix, int numberOfRows, int numberOfColumns, String explanation, Fraction multiple) {
        super(matrix, numberOfRows, numberOfColumns, explanation);
        this.multiple = multiple;
    }

    DeterminantStep(double[] matrix, int numberOfRows) {
        super(matrix, numberOfRows, numberOfRows, defaultExplanation);
        multiple = new Fraction(1);
    }

    @Override
    public String getData() {
        return multiple.toString() + "\n" + StringCreator.matrixToString(matrix);
    }

    Fraction getMultiple(){
        return multiple;
    }
}
