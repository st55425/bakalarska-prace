package exercise.determinant;

import exercise.IExercise;
import exercise.utils.Generator;
import org.ejml.simple.SimpleMatrix;


public final class DeterminantFactory {

    private DeterminantFactory(){}

    public static IExercise generateExercise(int numberOfRows, int max) {
        SimpleMatrix taskMatrix;
        do {
            taskMatrix = Generator.generateRegularMatrix(numberOfRows, -max, max);
        }while (Math.abs(taskMatrix.determinant()) > Math.pow(max, numberOfRows)/(numberOfRows*max));

        DeterminantStep taskStep = new DeterminantStep(taskMatrix.getDDRM().data, numberOfRows);
        return new DeterminantExercise(taskStep);
    }
}
