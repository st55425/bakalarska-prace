package exercise.determinant;

import exercise.*;

public class DeterminantExercise implements IExercise {

    private final DeterminantStep task;
    private final DeterminantSolver solver;

    public DeterminantExercise(DeterminantStep task) {
        this.task = task;
        solver = new DeterminantSolver(task);
    }

    @Override
    public String getTaskText() {
        return task.getExplanation();
    }

    @Override
    public IStep getTask() {
        return task;
    }

    @Override
    public IStep getSolution() {
        return solver.getSolution();
    }

    @Override
    public ISolutionIterator getFullSolution() {
        return new SolutionIterator(solver.getFullSolution());
    }

    @Override
    public Type getType() {
        return Type.DETERMINANT;
    }

    @Override
    public int getSize() {
        return Math.max(task.getSize(), getSolution().getSize());
    }
}
