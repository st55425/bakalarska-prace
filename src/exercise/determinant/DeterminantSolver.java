package exercise.determinant;

import exercise.Fraction;
import exercise.ISolver;
import exercise.IStep;
import exercise.utils.MathFunctions;

import java.util.ArrayList;
import java.util.List;

public class DeterminantSolver implements ISolver {
    private final List<IStep> steps;
    private Fraction multiple;
    private Fraction[][] lastMatrix;
    private DeterminantStep solution;

    @Override
    public List<IStep> getFullSolution(){
        return steps;
    }

    @Override
    public IStep getSolution(){
        return solution;
    }

    public DeterminantSolver(DeterminantStep task) {
        steps = new ArrayList<>();
        addStep(task);
        makeFullSolution();
    }

    private void addStep(DeterminantStep step){
        steps.add(step);
        solution = step;
        lastMatrix = step.getMatrix();
        multiple = step.getMultiple();
    }

    private void makeFullSolution() {
        for (int i = 0; i < lastMatrix.length; i++) {
            divideRowSteps(i);
            if (checkZeroedRow(i)){
                zeroSolution();
                return;
            }
        }

        for (int i = 0; i < lastMatrix.length-1; i++) {
            sortRows(i);
            int column = MathFunctions.findFirstNonZeroFraction(lastMatrix[i]);
            for (int j = i+1; j < lastMatrix.length; j++) {
                int[] rows = new int[2];
                if (MathFunctions.findLcmRowsByColumn(lastMatrix, i, column, true, rows)) {
                    nullNumberInRow(rows[0], rows[1], column);
                }
                if (checkZeroedRow(rows[1])){
                    zeroSolution();
                    return;
                }
            }
        }
        getDiagonal();
        getFinalSolution();
    }

    private void divideRowSteps(int row){
        int gcd =MathFunctions.divisorOfRow(lastMatrix, row);
        if (gcd !=1){
            Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
            if (MathFunctions.changeSignumOfRow(newMatrix, row)){
                gcd *=-1;
            }
            MathFunctions.divideRow2DArray(newMatrix, row,gcd);
            String explanation = "Vydělíme " + (row+1) + ". řádek číslem " + gcd + ", nezapomeneme celý determinant vynásobit stejným číslem.";

            addStep(new DeterminantStep(newMatrix, explanation, multiple.multiply(gcd)));
        }
    }

    private boolean checkZeroedRow(int row) {
        Fraction[] fractions = lastMatrix[row];
        for (Fraction f : fractions) {
            if (f.getNumerator() != 0) {
                return false;
            }
        }
        return true;
    }

    private void zeroSolution(){
        Fraction[][] matrix = new Fraction[1][1];
        matrix[0][0] = new Fraction(0);
        addStep(new DeterminantStep(matrix, "Byl nalezen nulový řádek, tudíž je matice singulární. Determinant singulární matice je roven 0.", new Fraction(1)));
    }

    private void sortRows(int row){
        Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
        String explanation = MathFunctions.switchMinRowByColumn(newMatrix, row);
        if (explanation.length() >1){
            addStep(new DeterminantStep(newMatrix, explanation + " Nezapomeneme změnit znaménko determinantu.", multiple.opposite()));
        }
    }

    private void nullNumberInRow(int controlRow, int nullableRow, int positionInRow){
        Fraction[][] newMatrix = MathFunctions.clone2DArray(lastMatrix);
        Fraction controlNumber = new Fraction(newMatrix[controlRow][positionInRow]);
        if (newMatrix[controlRow][positionInRow].getDenominator() == newMatrix[nullableRow][positionInRow].getDenominator()){
            int gcd = MathFunctions.calculateGCD(newMatrix[controlRow][positionInRow].getNumerator(), newMatrix[nullableRow][positionInRow].getNumerator());
            controlNumber = new Fraction(controlNumber.getNumerator()/gcd, controlNumber.getDenominator());
        }

        String explanation = MathFunctions.nullNumberInMatrix(newMatrix, controlRow, nullableRow, positionInRow);
        if (!controlNumber.isInteger() || (controlNumber.isInteger() && Math.abs(controlNumber.getNumerator()) != 1)){
            explanation += " Nezapomeneme celý determinant vydělit číslem " + controlNumber.abs() + ".";
        }

        addStep(new DeterminantStep(newMatrix, explanation, multiple.divide(controlNumber.abs())));
    }

    private void getDiagonal(){
        Fraction[][] newMatrix = new Fraction[1][lastMatrix.length];
        for (int i = 0; i < lastMatrix.length; i++) {
            newMatrix[0][i] = lastMatrix[i][i];
        }
        addStep(new DeterminantStep(newMatrix, "Vezmeme všechny prvky na hlavní diagonále, které mezi sebou vynásobíme, nezapomeneme na multiplikant determinantu.",
                multiple));
    }

    private void getFinalSolution(){
        double solve = multiple.toDouble();

        for (int i = 0; i < lastMatrix[0].length; i++) {
            solve *= lastMatrix[0][i].toDouble();
        }
        double[] solutionMatrix = new double[] {solve};
        addStep(new DeterminantStep(solutionMatrix,1,1, "Dopočítáme výsledek.", new Fraction(1)));
    }

}
