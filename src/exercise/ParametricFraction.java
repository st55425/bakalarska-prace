package exercise;


public class ParametricFraction extends Fraction {

    private final String parameterName;
    private final boolean parameterInNumerator;
    private final int exponent;
    private boolean fullText;

    public ParametricFraction(double number, String parameterName) {
        super(number);
        this.parameterName = parameterName;
        parameterInNumerator = true;
        fullText = false;
        exponent = 1;
    }

    public ParametricFraction(double number, String parameterName, boolean fullText) {
        this(number, parameterName);
        this.fullText = fullText;
    }

    public ParametricFraction(Fraction original) {
        super(original);
        if (original instanceof ParametricFraction){
            parameterName = ((ParametricFraction) original).parameterName;
            parameterInNumerator = ((ParametricFraction) original).parameterInNumerator;
            exponent = ((ParametricFraction) original).exponent;
            fullText = ((ParametricFraction) original).fullText;
        }else {
            parameterName = "t";
            parameterInNumerator = true;
            exponent = 1;
            fullText = false;
        }
    }

    public ParametricFraction(Fraction fraction, String parameterName, boolean parameterInNumerator, int exponent){
        super(fraction);
        this.parameterName = parameterName;
        this.parameterInNumerator = parameterInNumerator;
        fullText = false;
        this.exponent = exponent;
    }

    private ParametricFraction(int numerator, int denominator, String parameterName, boolean parameterInNumerator, int exponent){
        super(numerator, denominator);
        this.parameterName = parameterName;
        this.parameterInNumerator = parameterInNumerator;
        fullText = false;
        this.exponent = exponent;
    }


    @Override
    public Fraction add(Fraction second) {
        if (second instanceof ParametricFraction) {
            if (!this.parameterName.equals(((ParametricFraction) second).parameterName) || exponent != ((ParametricFraction) second).exponent){
                throw new IllegalArgumentException("Parametry zlomků jsou různé");
            }
            if ((this.parameterInNumerator == ((ParametricFraction) second).parameterInNumerator)) {
                int numerator = (this.numerator * second.denominator) +
                        (second.numerator * this.denominator);
                int denominator = this.denominator * second.denominator;
                return new ParametricFraction(numerator, denominator, this.parameterName, this.parameterInNumerator, exponent);
            } else {
                throw new UnsupportedOperationException();
            }
        }else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public Fraction subtract(Fraction second) {
        if (second instanceof ParametricFraction) {
            if (!this.parameterName.equals(((ParametricFraction) second).parameterName) || exponent != ((ParametricFraction) second).exponent){
                throw new IllegalArgumentException("Parametry zlomků jsou různé");
            }
            if ((this.parameterInNumerator == ((ParametricFraction) second).parameterInNumerator)) {
                int numerator = (this.numerator * second.denominator) -
                        (second.numerator * this.denominator);
                int denominator = this.denominator * second.denominator;
                return new ParametricFraction(numerator, denominator, this.parameterName, this.parameterInNumerator, exponent);
            } else {
                throw new UnsupportedOperationException();
            }
        }else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public Fraction multiply(Fraction second) {
        int numerator = this.numerator * second.numerator;
        int denominator = this.denominator * second.denominator;
        if (second instanceof ParametricFraction) {
            if (!this.parameterName.equals(((ParametricFraction) second).parameterName)){
                throw new IllegalArgumentException("Parametry zlomků jsou různé");
            }
            int exponent = this.exponent;
            if ((this.parameterInNumerator == ((ParametricFraction) second).parameterInNumerator)) {
                exponent += ((ParametricFraction) second).exponent;
            } else {
                exponent -= ((ParametricFraction) second).exponent;
            }
            if (exponent == 0){
                return new Fraction(numerator, denominator);
            }else if (exponent >0){
                return new ParametricFraction(numerator, denominator, this.parameterName, this.parameterInNumerator, exponent);
            }
            else {
                return new ParametricFraction(numerator, denominator, this.parameterName, !this.parameterInNumerator, Math.abs(exponent));
            }
        }else {
            return new ParametricFraction(numerator, denominator, this.parameterName, this.parameterInNumerator, exponent);
        }
    }

    @Override
    public Fraction divide(Fraction divisor) {
        return multiply(divisor.overturn());
    }

    @Override
    public boolean isInteger() {
        return denominator == 1 && parameterInNumerator;
    }

    @Override
    public Fraction abs() {
        return new ParametricFraction(super.abs(), parameterName, parameterInNumerator, exponent);
    }

    @Override
    public Fraction opposite() {
        return new ParametricFraction(super.opposite(),parameterName, parameterInNumerator, exponent);
    }

    @Override
    public Fraction overturn() {
        return new ParametricFraction(denominator, numerator, parameterName, !parameterInNumerator, exponent);
    }

    @Override
    public String toString() {
        if (fullText){
            return fulltextString();
        }
        if (numerator ==0){
            return "0";
        } else if (isInteger()){
            if (numerator == 1){
                return parameterWithExponent();
            }
            if (numerator == -1){
                return "-" + parameterWithExponent();
            }
            return numerator + parameterWithExponent();
        }else if(parameterInNumerator){
            return numerator + parameterWithExponent() + "/" + denominator;
        }else if (denominator ==1){
            return numerator + "/" + parameterWithExponent();
        }else {
            return numerator + "/" + denominator + parameterWithExponent();
        }
    }

    private String fulltextString(){
        if (denominator == 1 && parameterInNumerator){
            return numerator + parameterWithExponent();
        }
        else if (parameterInNumerator){
            return numerator + parameterWithExponent() + "/" + denominator;
        }
        else {
            return numerator + "/" + denominator + parameterWithExponent();
        }
    }

    private String parameterWithExponent(){
        if (exponent ==0){
            return "";
        }
        else if (exponent ==1){
            return parameterName;
        }
        else {
            return parameterName + "^" + exponent;
        }
    }

    public String getParameterName() {
        return parameterName;
    }

    public int getExponent(){
        return exponent;
    }

    public boolean isParameterInNumerator() {
        return parameterInNumerator;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ParametricFraction &&
                ((ParametricFraction) o).numerator == numerator &&
                ((ParametricFraction) o).denominator == denominator &&
                ((ParametricFraction) o).parameterInNumerator == parameterInNumerator &&
                ((ParametricFraction) o).parameterName.equals(parameterName) &&
                ((ParametricFraction) o).exponent == exponent ;
    }
}
