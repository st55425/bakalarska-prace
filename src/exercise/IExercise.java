package exercise;

public interface IExercise {

    String getTaskText();

    IStep getTask();

    IStep getSolution();

    ISolutionIterator getFullSolution();

    Type getType();

    int getSize();

}
