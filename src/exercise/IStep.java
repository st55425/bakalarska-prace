package exercise;

public interface IStep {
    String getData();

    String getExplanation();

    int getSize();
}
