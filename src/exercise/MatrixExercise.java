package exercise;

public abstract class MatrixExercise implements IExercise {

    protected final MatrixStep task;
    protected MatrixSolver solver;

    public MatrixExercise(MatrixStep task) {
        this.task = task;
    }

    @Override
    public String getTaskText() {
        return task.getExplanation();
    }

    @Override
    public IStep getTask() {
        return task;
    }

    @Override
    public IStep getSolution() {
        return solver.getSolution();
    }

    @Override
    public ISolutionIterator getFullSolution() {
        return new SolutionIterator(solver.getFullSolution());
    }

    @Override
    public int getSize() {
        return Math.max(task.getSize(), getSolution().getSize());
    }

}
