package exercise;

import java.util.List;

public interface ISolver {
    List<IStep> getFullSolution();

    IStep getSolution();
}
