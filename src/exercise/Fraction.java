package exercise;

import exercise.utils.MathFunctions;

public class Fraction {

    protected int numerator;
    protected int denominator;

    public Fraction(int numerator, int denominator) {
        if (denominator == 0){
            throw new ArithmeticException("Nelze vytvořit zlomek s dělitelem 0");
        }
        this.numerator = numerator;
        this.denominator = denominator;
        reduce();
    }

    public Fraction(double number) {
        if (MathFunctions.isInteger(number, 15)){
            numerator = (int)Math.round(number);
            denominator = 1;
        }
        else{
            double maximumMistake = 0.00000000000000001;
            int maxIterations = 10000;
            double realNumber = number;
            long firstLong = (long)Math.floor(number);
            long tmpNumerator = 1;
            long tmpDenominator = 0;
            long previousNumerator = firstLong;
            long previousDenominator = 1;

            long lastNumerator;
            long lastDenominator;

            int n = 0;
            boolean stop = false;
            do {
                ++n;
                double lastRealNumber = 1.0 / (realNumber - firstLong);
                long a1 = (long)Math.floor(lastRealNumber);
                lastNumerator = (a1 * previousNumerator) + tmpNumerator;
                lastDenominator = (a1 * previousDenominator) + tmpDenominator;

                if ((Math.abs(lastNumerator) > Integer.MAX_VALUE) || (Math.abs(lastDenominator) > Integer.MAX_VALUE)) {
                    break;
                }
                double convergent = (double)lastNumerator / (double)lastDenominator;
                if (n < maxIterations && Math.abs(convergent - number) > maximumMistake && lastDenominator < Integer.MAX_VALUE) {
                    tmpNumerator = previousNumerator;
                    previousNumerator = lastNumerator;
                    tmpDenominator = previousDenominator;
                    previousDenominator = lastDenominator;
                    firstLong = a1;
                    realNumber = lastRealNumber;
                } else {
                    stop = true;
                }
            } while (!stop);

            if (lastDenominator < Integer.MAX_VALUE) {
                this.numerator = (int) lastNumerator;
                this.denominator = (int) lastDenominator;
            } else {
                this.numerator = (int) previousNumerator;
                this.denominator = (int) previousDenominator;
            }
        }
    }

    public Fraction(Fraction original) {
        this.denominator = original.denominator;
        this.numerator = original.numerator;
        reduce();
    }

    private void reduce(){
        int gcd = MathFunctions.calculateGCD(numerator, denominator);
        numerator /= Math.abs(gcd);
        denominator /= Math.abs(gcd);
        if (denominator < 0){
            numerator *= -1;
            denominator *= -1;
        }else if (Math.signum(numerator) == Math.signum(denominator)){
            numerator = Math.abs(numerator);
            denominator = Math.abs(denominator);
        }
    }

    public Fraction add(Fraction second) {
        if (second instanceof ParametricFraction){
            return second.add(this);
        }
        int numerator = (this.numerator * second.denominator) +
                (second.numerator * this.denominator);
        int denominator = this.denominator * second.denominator;
        return new Fraction(numerator, denominator);
    }

    public Fraction subtract(Fraction second) {
        if (second instanceof ParametricFraction){
            return second.opposite().add(this);
        }
        int numerator = (this.numerator * second.denominator) -
                (second.numerator * this.denominator);
        int denominator = this.denominator * second.denominator;
        return new Fraction(numerator, denominator);
    }

    public Fraction multiply(Fraction second) {
        if (second instanceof ParametricFraction){
            return second.multiply(this);
        }
        int numerator = this.numerator * second.numerator;
        int denominator = this.denominator * second.denominator;
        return new Fraction(numerator, denominator);
    }

    public Fraction multiply(double second){
        Fraction fraction = new Fraction(second);
        return this.multiply(fraction);
    }

    public Fraction divide(Fraction divisor) {
        if (divisor instanceof ParametricFraction){
            divisor.multiply(divisor.overturn());
        }
        return this.multiply(divisor.overturn());
    }

    public Fraction divide(double divisor) {
        Fraction fraction = new Fraction(divisor);
        return this.divide(fraction);
    }

    public Fraction overturn(){
        return new Fraction(this.denominator, this.numerator);
    }

    public boolean isInteger(){
        return denominator ==1;
    }

    public Fraction abs(){
        return new Fraction(Math.abs(numerator), Math.abs(denominator));
    }

    public Fraction opposite(){
        return new Fraction(this.numerator*-1, this.denominator);
    }

    @Override
    public String toString() {
        if (isInteger()){
            return String.valueOf(numerator);
        }
        return numerator + "/" + denominator;
    }

    public double toDouble(){
        return (double)numerator/denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Fraction &&
                ((Fraction) o).numerator == numerator &&
                ((Fraction) o).denominator == denominator;
    }
}
