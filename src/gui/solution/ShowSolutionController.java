package gui.solution;

import exercise.ISolutionIterator;
import exercise.IStep;
import gui.Main;
import gui.formatStep.IFormatter;
import io.github.egormkn.LatexView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class ShowSolutionController extends BorderPane {

    @FXML
    private BorderPane solutionBorderPane;

    @FXML
    private AnchorPane descriptionAnchorPane;

    @FXML
    private Label descriptionLabel;

    @FXML
    private LatexView stepsCanvas;



    public void setupEnvironment(IStep step, IFormatter formatter){
        stepsCanvas.setFormula(formatter.format(step));
        stepsCanvas.setSize(20);
        descriptionLabel.setText(step.getExplanation());
    }

}
