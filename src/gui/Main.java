package gui;

import gui.mainWindow.MainWindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static MainWindowController mainController;
    /*
    Licence knihoven:
    latexview gpl v3
    jlatexmath gpl v2 nebo vyšší
    ejml apache v2
     */

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(MainWindowController.class.getResource("MainWindow.fxml"));
        Parent root = loader.load();
        mainController = loader.getController();
        primaryStage.setTitle("Výuková aplikace pro lineární algebru");
        primaryStage.setScene(new Scene(root));
        primaryStage.setMinHeight(700);
        primaryStage.setMinWidth(1000);
        primaryStage.show();
    }

    public static MainWindowController getMainController() {
        return mainController;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
