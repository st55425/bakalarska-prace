package gui.mainWindow;

import exercise.IExercise;
import exercise.ISolutionIterator;
import exercise.IStep;
import exercise.Type;
import exercise.determinant.DeterminantFactory;
import exercise.eigen.EigenFactory;
import exercise.gem.GEMFactory;
import exercise.gem.inversion.InversionFactory;
import exercise.gem.vector.CombinationFactory;
import exercise.gem.vector.DependencyFactory;
import exercise.sarrus.SarrusFactory;
import gui.exercises.ShowExerciseController;
import gui.formatStep.FormatterFactory;
import gui.formatStep.IFormatter;
import gui.solution.ShowSolutionController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {


    @FXML
    private ComboBox<Type> typeComboBox;

    @FXML
    private Spinner<Integer> numExercisesSpinner;

    @FXML
    private Spinner<Integer> numVariablesSpinner;

    @FXML
    private Spinner<Integer> rangeSpinner;

    @FXML
    private Label numVariablesLabel;

    @FXML
    private GridPane stepsGridPane;

    @FXML
    private Button generateButton;

    @FXML
    private Button stepBackButton;

    @FXML
    private Button closeSolutionButton;

    @FXML
    private Button stepForwardButton;

    @FXML
    private ScrollPane mainScrollPane;

    @FXML
    private FlowPane mainFlowPane;

    @FXML
    private BorderPane scrollBorderPane;

    private ISolutionIterator solution;
    private IFormatter formatter;
    private IStep currentStep;
    private List<Node> exercises;

    private static final int START_EXERCISE_SIZE = 150;
    private static final int EXERCISE_SIZE_MULTIPLY = 16;

    private static final int START_STEP_SIZE = 95;
    private static final int STEP_SIZE_MULTIPLY = 23;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainFlowPane.prefWidthProperty().bind(mainScrollPane.widthProperty());
        mainScrollPane.prefWidthProperty().bind(scrollBorderPane.widthProperty());
        ObservableList<Type> comboBoxItems = FXCollections.observableArrayList(Type.class.getEnumConstants());
        typeComboBox.setItems(comboBoxItems);
        typeComboBox.getSelectionModel().select(Type.GEM);

        numExercisesSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1,10,1));
        numVariablesSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(2,5,3));
        rangeSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(5,15,10));

        mainFlowPane.minHeightProperty().bind(mainScrollPane.heightProperty().subtract(2));

        generateExercises(1);
        exercises = new ArrayList<>();
        exercises.addAll(mainFlowPane.getChildren());

        stepsGridPane.setPrefHeight(0);
        stepsGridPane.setVisible(false);

    }
    @FXML
    void generateButtonOnAction(ActionEvent event) {
        stepsGridPane.setPrefHeight(0);
        stepsGridPane.setVisible(false);
        mainFlowPane.getChildren().clear();
        exercises.clear();
        mainScrollPane.setContent(mainFlowPane);
        generateExercises(numExercisesSpinner.getValue());
        exercises.addAll(mainFlowPane.getChildren());

    }

    @FXML
    private void typeComboBoxOnAction(ActionEvent event) {
        if (typeComboBox.getSelectionModel().getSelectedItem()==Type.SARRUS){
            numVariablesSpinner.setVisible(false);
            numVariablesLabel.setVisible(false);
        }else {
            numVariablesSpinner.setVisible(true);
            numVariablesLabel.setVisible(true);
        }
        if (typeComboBox.getSelectionModel().getSelectedItem()==Type.GEM){
            numVariablesLabel.setText("Počet proměnných: ");
        }
        else if (typeComboBox.getSelectionModel().getSelectedItem()==Type.COMBINATION ||typeComboBox.getSelectionModel().getSelectedItem()==Type.DEPENDENCY){
            numVariablesLabel.setText("Počet vektorů: ");
        }
        else {
            numVariablesLabel.setText("Velikost matice: ");

        }
        if (typeComboBox.getSelectionModel().getSelectedItem()==Type.EIGEN){
            ((SpinnerValueFactory.IntegerSpinnerValueFactory)numVariablesSpinner.getValueFactory()).setMax(4);
        }else {
            ((SpinnerValueFactory.IntegerSpinnerValueFactory)numVariablesSpinner.getValueFactory()).setMax(5);
        }
    }

    @FXML
    private void closeSolutionButtonOnAction(ActionEvent event) {
        mainFlowPane.getChildren().clear();
        mainFlowPane.getChildren().addAll(exercises);
        stepsGridPane.setPrefHeight(0);
        stepsGridPane.setVisible(false);
    }

    @FXML
    private void stepBackButtonOnAction(ActionEvent event) {
        if (solution.hasPreviousStep()){
            currentStep = solution.previousStep();
            if (mainFlowPane.getChildren().size()>0){
                mainFlowPane.getChildren().remove(mainFlowPane.getChildren().size()-1);
            }
            stepForwardButton.setVisible(true);
            stepBackButton.setVisible(false);
        }
        if (solution.hasPreviousStep()){
            stepBackButton.setVisible(true);
        }
    }

    @FXML
    private void stepForwardButtonOnAction(ActionEvent event) {
        if (solution.hasNextStep()){
            currentStep = solution.nextStep();
            createExerciseStep();
            stepBackButton.setVisible(true);
            stepForwardButton.setVisible(false);
        }
        if (solution.hasNextStep()){
            stepForwardButton.setVisible(true);
        }
    }


    private IExercise generateExercise(){
        switch (typeComboBox.getSelectionModel().getSelectedItem()){
            case GEM:
                return GEMFactory.generateExercise(numVariablesSpinner.getValue(), rangeSpinner.getValue());
            case DETERMINANT:
                return DeterminantFactory.generateExercise(numVariablesSpinner.getValue(), rangeSpinner.getValue());
            case INVERSION:
                return InversionFactory.generateExercise(numVariablesSpinner.getValue(), rangeSpinner.getValue());
            case SARRUS:
                return SarrusFactory.generateExercise(rangeSpinner.getValue());
            case COMBINATION:
                return CombinationFactory.generateExercise(numVariablesSpinner.getValue(), rangeSpinner.getValue());
            case DEPENDENCY:
                return DependencyFactory.generateExercise(numVariablesSpinner.getValue(), rangeSpinner.getValue());
            case EIGEN:
                return EigenFactory.generateExercise(numVariablesSpinner.getValue(), rangeSpinner.getValue());
            default:
                throw new RuntimeException("Výběr tématu je neplatný");
        }
    }

    private void generateExercises(int numberOfExercises){
        for (int i = 0; i < numberOfExercises; i++) {
            FXMLLoader loader = new FXMLLoader(ShowExerciseController.class.getResource("ShowExercise.fxml"));
            try {
                Pane pane = loader.load();
                ShowExerciseController controller = loader.getController();
                IExercise exercise = generateExercise();
                pane.setPrefHeight(START_EXERCISE_SIZE+ exercise.getSize()* EXERCISE_SIZE_MULTIPLY);
                controller.setExercise(exercise);
                mainFlowPane.getChildren().add(pane);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setupExerciseSteps(IExercise exercise){
        this.solution = exercise.getFullSolution();
        this.formatter = FormatterFactory.getFormatter(exercise.getType());
        mainFlowPane.getChildren().clear();
        stepsGridPane.setPrefHeight(40);
        stepsGridPane.setVisible(true);
        stepBackButton.setVisible(false);
        if (solution.hasNextStep()){
            currentStep = solution.nextStep();
            createExerciseStep();
        }
        if (solution.hasNextStep()){
            stepForwardButton.setVisible(true);
        }else {
            stepForwardButton.setVisible(false);
        }
    }

    private void createExerciseStep(){
        Thread thread = new Thread(() -> {
            try {
                FXMLLoader loader = new FXMLLoader(ShowSolutionController.class.getResource("ShowSolution.fxml"));
                Pane solutionStep = loader.load();
                ShowSolutionController controller = loader.getController();
                solutionStep.setPrefHeight(START_STEP_SIZE+ currentStep.getSize()* STEP_SIZE_MULTIPLY);
                controller.setupEnvironment(currentStep, formatter);
                solutionStep.prefWidthProperty().bind(mainScrollPane.widthProperty());
                Platform.runLater(() ->mainFlowPane.getChildren().add(solutionStep));
                Thread.sleep(50);
                Platform.runLater(() -> mainScrollPane.setVvalue(1.0));
            } catch (IOException | InterruptedException exc) {
                exc.printStackTrace();
            }
        });
        thread.start();

    }
}
