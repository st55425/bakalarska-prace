package gui.formatStep;

import exercise.IStep;

public class GEMFormatter implements IFormatter {

    private static GEMFormatter instance = null;

    private GEMFormatter() {}

    static GEMFormatter getInstance() {
        if (instance == null) {
                    instance = new GEMFormatter();
            }
        return instance;
    }

    @Override
    public String format(IStep step) {
        String[] rows = step.getData().split("\n");
        if (FormatterUtil.containsParameter(rows[0])){
            return FormatterUtil.formatToParametricMatrix(rows);
        }
        return FormatterUtil.formatToAugmentedMatrix(rows, 1, step.getExplanation());
    }

    @Override
    public String formatSolution(IStep step) {
        return FormatterUtil.formatToParametricMatrix(step.getData().split("\n"));
    }


}
