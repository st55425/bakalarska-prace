package gui.formatStep;

import java.util.ArrayList;
import java.util.List;

final class FormatterUtil {

    private FormatterUtil() {
    }

    static String formatToMatrix(String[] rows, boolean colorRows, int controlRow, int changedRow) {
        StringBuilder stringMatrixFormula = new StringBuilder();
        for (int i = 0; i < rows.length; i++) {
            String[] numbers = rows[i].split(" ");
            for (int j = 0; j < numbers.length; j++) {
                if (colorRows && i==changedRow){
                    stringMatrixFormula.append("\\textcolor{red}{");
                } else if (colorRows && i==controlRow){
                    stringMatrixFormula.append("\\textcolor{blue}{");
                }
                    stringMatrixFormula.append(formatFraction(numbers[j]));
                if (colorRows && (i==controlRow || i==changedRow)){
                    stringMatrixFormula.append("}");
                }
                if (j < (numbers.length - 1)) {
                    stringMatrixFormula.append(" & ");
                }
            }
            stringMatrixFormula.append(" \\\\ ");
        }

        return stringMatrixFormula.toString();
    }

    static String formatToMatrix(String[] rows, boolean colorMainDiagonal, boolean colorSecondaryDiagonal, int offset){
        StringBuilder stringFormula = new StringBuilder();
        for (int i = 0; i < rows.length; i++) {
            String[] numbers = rows[i].split(" ");
            for (int j = 0; j < numbers.length; j++) {
                boolean color = (colorMainDiagonal && i == j + offset) || (colorSecondaryDiagonal && i+j == numbers.length-1+offset);
                if (color) {
                    stringFormula.append("\\textcolor{red}{");
                }
                stringFormula.append(formatFraction(numbers[j]));
                if (color) {
                    stringFormula.append("}");
                }
                if (j < (numbers.length - 1)) {
                    stringFormula.append(" & ");
                }
            }
            stringFormula.append(" \\\\ ");
        }
        return stringFormula.toString();
    }

    static String formatToAugmentedMatrix(String[] rows, int rightColumns, String explanation) {
        StringBuilder stringMatrixFormula = new StringBuilder();
        if (explanation.contains("nemá řešení")){
            stringMatrixFormula.append("\\{ \\}");
            return stringMatrixFormula.toString();
        }
        int[] rowsToColor = new int[]{-1,-1};
        boolean colorRows = colorRows(explanation, rowsToColor);
        boolean colorDiagonal = explanation.contains("λ");
        int numberOfColumns = rows[0].split(" ").length;
        stringMatrixFormula.append("\\left(\\begin{array}{");
        for (int i = 0; i < numberOfColumns; i++) {
            if (i == (numberOfColumns - rightColumns)) {
                stringMatrixFormula.append("|");
            }
            stringMatrixFormula.append("c");

        }
        stringMatrixFormula.append("}");
        if (colorDiagonal){
            stringMatrixFormula.append(formatToMatrix(rows, true, false, 0));
        }else {
            stringMatrixFormula.append(formatToMatrix(rows, colorRows, rowsToColor[1], rowsToColor[0]));
        }
        stringMatrixFormula.append("\\end{array}\\right)\\");
        return stringMatrixFormula.toString();
    }

    private static boolean colorRows(String explanation, int[] rows){
        if (explanation.matches(".*[0-9][.].*")){
            String[] splitByDot = explanation.split("\\.");
            int rowsCounter = 0;
            for (int i = 0; i < splitByDot.length-1; i++) {
                if (Character.isDigit(splitByDot[i].charAt(splitByDot[i].length() - 1))) {
                    rows[rowsCounter++] = Character.getNumericValue(splitByDot[i].charAt(splitByDot[i].length() - 1)) - 1;
                    if (rowsCounter >= 2) {
                        break;
                    }
                }
            }
            return true;
        }
        return false;
    }

    static String formatFraction(String fraction) {
        if (fraction.contains("/")) {
            String[] numbers = fraction.split("/");
            if (numbers[0].matches(".*[a-z].*")) {
                if (numbers[0].charAt(0) == '-' && numbers[0].length() > 2) {
                    if (numbers[0].charAt(1) == '1' && Character.isLetter(numbers[0].charAt(2))) {
                        numbers[0] = "-" + numbers[0].charAt(2);
                    }
                } else {
                    if (numbers[0].charAt(0) == '1' && Character.isLetter(numbers[0].charAt(1))) {
                        numbers[0] = numbers[0].substring(1);
                    }
                }
            }
            if (numbers[1].equals("1")) {
                return numbers[0];
            }
            return "\\[\\frac{" + numbers[0] + "}{" + numbers[1].replace("-","") + "}\\]";
        }
        return fraction;
    }

    static String formatToDeterminant(String[] rows, String explanation) {
        int[] rowsToColor = new int[2];
        boolean colorRows = colorRows(explanation, rowsToColor);
        String stringFormula = "";
        stringFormula += "\\begin{vmatrix}";
        stringFormula += FormatterUtil.formatToMatrix(rows, colorRows, rowsToColor[1], rowsToColor[0]);
        stringFormula += "\\end{vmatrix}";
        return stringFormula;
    }

    static String formatRowAsMultipliers(String[] fractions, boolean firstWithBrackets) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < fractions.length; i++) {
            if ((i != 0 || firstWithBrackets) && fractions[i].charAt(0) == '-') {
                result.append("(");
            }

            result.append(formatFraction(fractions[i]));

            if ((i != 0 || firstWithBrackets) && fractions[i].charAt(0) == '-') {
                result.append(")");
            }
            if (i != fractions.length - 1) {
                result.append("\\times");
            }
        }
        return result.toString();
    }

    static String formatRowToSum(String[] fractions) {
        StringBuilder result = new StringBuilder();
        if (!fractions[0].equals("0")) {
            result.append(FormatterUtil.formatFraction(fractions[0]));
        }
        for (int i = 1; i < fractions.length; i++) {
            if (fractions[i].equals("0")) {
                continue;
            } else if (fractions[i].equals("=")) {
                result.append("=");
            }
            else if (fractions[i].contains("-")) {
                result.append(" - ");
                result.append(FormatterUtil.formatFraction(fractions[i].replace("-", "")));
            } else {
                if (result.length() > 0) {
                    result.append(" + ");
                }
                result.append(FormatterUtil.formatFraction(fractions[i]));
            }
        }
        if (result.length()>0 && result.charAt(result.length()-1) == '='){
            result.append("0");
        }
        return result.toString();
    }

    static String formatToParametricMatrix(String[] rows) {
        StringBuilder stringFormula = new StringBuilder();
        for (int i = 0; i < rows.length; i++) {
            String row = rows[i].replace("0 ", "");
            String[] numbers = row.split(" ");
            for (int j = 0; j < numbers.length; j++) {
                if (numbers[j].equals("0") && (j == 0 || !numbers[j - 1].equals("="))) {
                    stringFormula.append(" ");
                } else {
                    if (j != 0 && !numbers[j].equals("=") && !numbers[j - 1].equals("=") && !numbers[j - 1].equals("0")) {
                        if (numbers[j].charAt(0) == '-') {
                            numbers[j] = numbers[j].substring(1);
                            stringFormula.append(" - ");
                        } else {
                            stringFormula.append(" + ");
                        }
                    }
                    stringFormula.append(formatFraction(numbers[j]));
                }
                if (j == numbers.length - 1 && numbers[j].equals("=")) {
                    stringFormula.append(" 0");
                }
            }
            if (i < (rows.length - 1)) {
                stringFormula.append(" \\\\ ");
            }
        }
        if (stringFormula.length()==0){
            stringFormula.append("\\{ \\}");

        }
        return stringFormula.toString();
    }

    static boolean containsParameter(String row) {
        for (char character : row.toCharArray()) {
            if (Character.isLetter(character)) {
                return true;
            }
        }
        return false;
    }

    static boolean containsParameter(String[] rows){
        for (String row:rows) {
            if (containsParameter(row)){
                return true;
            }
        }
        return false;
    }

    static String formatToVectors(String[] rows) {
        StringBuilder stringFormula = new StringBuilder();
        stringFormula.append("\\begin{array}(");
        for (String row : rows) {
            String[] fractions = row.split(" ");
            for (int j = 0; j < 2; j++) {
                stringFormula.append(fractions[j]);
                stringFormula.append("&");
            }
            stringFormula.append("(");
            for (int j = 2; j < fractions.length - 1; j++) {
                stringFormula.append(fractions[j]);
                stringFormula.append("&");
            }
            stringFormula.append(fractions[fractions.length - 1]);
            stringFormula.append(")");
            stringFormula.append("\\\\");
        }
        stringFormula.append("\\end{array}");
        return stringFormula.toString();
    }


    static String formatToCombination(String row) {
        row = row.trim();
        StringBuilder result = new StringBuilder();
        String[] fractions = row.split(" ");

        if (fractions.length < 3) {
            result.append("\\text{");
            result.append(fractions[0]);
            result.append(" ");
            result.append(fractions[1]);
            result.append(" ");
            result.append("}");
        }else{
            List<String> coefficients = new ArrayList<>();
            int equalsPossition = 0;
            for (int i = 0; i < fractions.length; i++) {
                if (fractions[i].equals("=")){
                    result.append(fractions[i]);
                    equalsPossition = i;
                }
                else if (fractions[i].matches(".*[A-Z].*")) {
                    if (coefficients.size() > 1) {
                        if (i - coefficients.size() > 1 && i - coefficients.size() > equalsPossition+1) {
                            result.append(" +");
                        }
                        result.append(" (");
                        result.append(formatFraction(coefficients.remove(0)));
                        for (String fraction : coefficients) {
                            if (fraction.contains("-")) {
                                result.append(" - ");
                                fraction = fraction.replace("-", "");
                            } else {
                                result.append(" + ");
                            }
                            result.append(formatFraction(fraction));
                        }
                        coefficients.clear();
                        result.append(") ");
                    } else if (coefficients.size()>0){
                        if (coefficients.get(0).contains("-") && i > 2 && i >equalsPossition+2) {
                            result.append(" - ");
                            coefficients.set(0, coefficients.get(0).replace("-", ""));
                        } else if (i > 2 && i >equalsPossition+2) {
                            result.append(" + ");
                        }
                        result.append(formatFraction(coefficients.get(0)));
                        coefficients.clear();
                    }
                    result.append(fractions[i]);
                } else {
                    coefficients.add(fractions[i]);
                }
            }
            if (!fractions[fractions.length-1].matches(".*[A-Z].*")){
                result.append(formatFraction(fractions[fractions.length-1]));
            }
        }
        return result.toString();
    }

}
