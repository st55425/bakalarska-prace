package gui.formatStep;

import exercise.IStep;

public class InversionFormatter implements IFormatter {
    private static InversionFormatter instance = null;

    private InversionFormatter() {}

    static InversionFormatter getInstance() {
        if (instance == null) {
            instance = new InversionFormatter();
        }
        return instance;
    }


    @Override
    public String format(IStep step) {
        String[] rows = step.getData().split("\n");
        if (rows[0].split(" ").length == rows.length){
            return FormatterUtil.formatToAugmentedMatrix(rows, 0, step.getExplanation());
        }
        else {
            return FormatterUtil.formatToAugmentedMatrix(rows, rows.length, step.getExplanation());
        }

    }

    @Override
    public String formatSolution(IStep step) {
        return FormatterUtil.formatToAugmentedMatrix(step.getData().split("\n"), 0, step.getExplanation());
    }


}
