package gui.formatStep;

import exercise.IStep;

public class VectorFormatter implements IFormatter {

    private static VectorFormatter instance = null;

    private VectorFormatter() {}

    static VectorFormatter getInstance() {
        if (instance == null) {
            instance = new VectorFormatter();
        }
        return instance;
    }

    @Override
    public String format(IStep step) {
        String[] rows = step.getData().split("\n");
        if (step.getExplanation().contains("nemá řešení")){
            return "\\{\\}";
        }
        if (rows.length ==1 && (rows[0].matches(".*[A-Z].*") || rows[0].charAt(0) =='0')){
            return FormatterUtil.formatToCombination(rows[0]);
        }
        else if (rows[0].contains("A")){
            return FormatterUtil.formatToVectors(rows);
        }
        else if (FormatterUtil.containsParameter(step.getData())){
            return FormatterUtil.formatToParametricMatrix(rows);
        }
        return FormatterUtil.formatToAugmentedMatrix(rows, 1, step.getExplanation());
    }

    @Override
    public String formatSolution(IStep step) {
        return FormatterUtil.formatToCombination(step.getData());
    }


}