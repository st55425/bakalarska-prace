package gui.formatStep;

import exercise.IStep;

public interface IFormatter {

    String  format(IStep step);

    String formatSolution(IStep step);
}
