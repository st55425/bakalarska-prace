package gui.formatStep;

import exercise.IStep;

import java.util.Arrays;

public final class DeterminantFormatter implements IFormatter {

    private static DeterminantFormatter instance = null;

    private DeterminantFormatter() {}

    static DeterminantFormatter getInstance() {
        if (instance == null) {
            instance = new DeterminantFormatter();
        }
        return instance;
    }

    @Override
    public String format(IStep step) {
        String[] rows = step.getData().split("\n");
        if (rows.length ==2){
            return formatSolution(rows);
        }
        String stringFormula = "";
        if (rows[0].equals("-1")){
            stringFormula += "-";
        }else if (!rows[0].equals("1")){
            stringFormula += FormatterUtil.formatFraction(rows[0]);
        }
        stringFormula += FormatterUtil.formatToDeterminant(Arrays.copyOfRange(rows, 1, rows.length), step.getExplanation());
        return stringFormula;
    }

    @Override
    public String formatSolution(IStep step) {
        return formatSolution(step.getData().split("\n"));
    }


    private String formatSolution(String[] rows){
        String stringFormula = "";
        if (rows[0].equals("-1")){
            stringFormula += "-(";
        }else if (!rows[0].equals("1")){
            stringFormula += FormatterUtil.formatFraction(rows[0]);
            stringFormula += "\\times";
        }
        String[] numbers = rows[1].split(" ");
        boolean firstWithBrackets = !(rows[0].equals("1") || rows[0].equals("-1"));
        stringFormula += FormatterUtil.formatRowAsMultipliers(numbers, firstWithBrackets);
        if (rows[0].equals("-1")){
            stringFormula += ")";
        }
        return stringFormula;
    }
}
