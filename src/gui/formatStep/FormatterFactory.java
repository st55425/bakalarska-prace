package gui.formatStep;

import exercise.Type;

public final class FormatterFactory {

    private FormatterFactory(){}

    public static IFormatter getFormatter(Type formatterType){
        IFormatter formatter = null;
        switch (formatterType){
            case GEM:
                formatter = GEMFormatter.getInstance();
                break;
            case DETERMINANT:
                formatter = DeterminantFormatter.getInstance();
                break;
            case INVERSION:
                formatter = InversionFormatter.getInstance();
                break;
            case SARRUS:
                formatter = SarrusFormatter.getInstance();
                break;
            case COMBINATION:
            case DEPENDENCY:
                formatter = VectorFormatter.getInstance();
                break;
            case EIGEN:
                formatter = EigenFormatter.getInstance();

        }
        return formatter;
    }
}
