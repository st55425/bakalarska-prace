package gui.formatStep;

import exercise.IStep;

import java.util.Arrays;

public class SarrusFormatter implements IFormatter {

    private static SarrusFormatter instance = null;

    private SarrusFormatter() {}

    static SarrusFormatter getInstance() {
        if (instance == null) {
            instance = new SarrusFormatter();
        }
        return instance;
    }


    @Override
    public String format(IStep step) {
        String[] rows = step.getData().split("\n");
        if (rows.length >1){
            return formatToDeterminant(rows, step.getExplanation());
        }else {
            String[] fractions = rows[0].split(" ");
            if (step.getExplanation().contains("výsledek")){
                return FormatterUtil.formatFraction(fractions[0]);
            }
            else {
                return formatToIntermediateSolution(fractions, step.getExplanation());
            }
        }
    }

    @Override
    public String formatSolution(IStep step) {
        return format(step);
    }

    private String formatToDeterminant(String[] rows, String explanation){
        if(explanation.contains("diagonále")) {
            StringBuilder stringFormula = new StringBuilder();
            stringFormula.append("\\begin{vmatrix}");
            int offset = 0;
            if (explanation.contains("1")){
                offset = 1;
            }
            else if (explanation.contains("2")){
                offset = 2;
            }
            if (explanation.contains("hlavní")){
                stringFormula.append(FormatterUtil.formatToMatrix(rows, true, false, offset));
            }else {
                stringFormula.append(FormatterUtil.formatToMatrix(rows, false, true, offset));
            }
            stringFormula.append("\\end{vmatrix}");
            return stringFormula.toString();
        }else {
            return FormatterUtil.formatToDeterminant(rows, explanation);
        }
    }

    private String formatToIntermediateSolution(String[] fractions, String explanation){
        StringBuilder stringFormula = new StringBuilder();
        if (explanation.contains("později")){
            stringFormula.append(FormatterUtil.formatRowAsMultipliers(Arrays.copyOfRange(fractions, 1, fractions.length-2), false));
        }else {
            stringFormula.append(FormatterUtil.formatFraction(fractions[0]));
            if (explanation.contains("přičteme")){
                stringFormula.append("+");
            }else {
                stringFormula.append("-");
            }
            stringFormula.append("(");
            stringFormula.append(FormatterUtil.formatRowAsMultipliers(Arrays.copyOfRange(fractions, 1, fractions.length - 2), false));
            stringFormula.append(")");
        }
        for (int i = 2; i > 0; i--) {
            stringFormula.append(FormatterUtil.formatFraction(fractions[fractions.length - i]));
        }
        return stringFormula.toString();
    }

}
