package gui.formatStep;

import exercise.IStep;

public final class EigenFormatter implements IFormatter{

    private static EigenFormatter instance = null;

    private EigenFormatter() {}

    static EigenFormatter getInstance() {
        if (instance == null) {
            instance = new EigenFormatter();
        }
        return instance;
    }

    @Override
    public String format(IStep step) {
        String[] rows = step.getData().split("\n");
        if (!rows[0].contains("λ_0") && rows[0].contains("x_0")){
            return formatVectors(rows);
        }
        else if (rows[0].contains("λ_0")){
            return formatSolution(rows, false);
        }
        else if (rows.length ==1 && rows[0].contains("λ")){
            return FormatterUtil.formatRowToSum(rows[0].split(" "));
        }
        else if (rows[0].matches(".*λ.*λ.*")){
            return formatToPolynom(rows);
        }
        else if (rows[0].contains("-λ")){
            return FormatterUtil.formatToDeterminant(rows, step.getExplanation());
        }
        else if (FormatterUtil.containsParameter(rows)){
            return FormatterUtil.formatToParametricMatrix(rows);
        }
        else if (rows[0].split(" ").length != rows.length){
            return FormatterUtil.formatToAugmentedMatrix(rows,1, step.getExplanation());
        }
        return FormatterUtil.formatToAugmentedMatrix(rows,0, step.getExplanation());
    }

    @Override
    public String formatSolution(IStep step) {
        return formatSolution(step.getData().split("\n"), true);
    }


    private String formatToPolynom(String[] rows){
        StringBuilder stringFormula = new StringBuilder();
        for (int i = 0; i < rows.length; i++) {
            String[] fractions = rows[i].split(" ");
            boolean zero = false;
            for (String fraction:fractions) {
                if (fraction.equals("0")){
                    zero = true;
                    break;
                }
            }
            if (zero){
                continue;
            }
            if (i>0){
                if (i < rows.length/2){
                    if (rows[i].charAt(0) == '-'){
                        stringFormula.append("-\\\\");
                        rows[i] = rows[i].substring(1);
                    }
                    else {
                        stringFormula.append("+\\\\");
                    }
                }else {
                    if (rows[i].charAt(0) == '-'){
                        stringFormula.append("+\\\\");
                        rows[i] = rows[i].substring(1);
                    }
                    else {
                        stringFormula.append("-\\\\");
                    }
                }
            }
            for (int j = 0; j < fractions.length; j++) {
                fractions[j] = fractions[j].replace("0", "");
                if (fractions[j].contains("-")) {
                    stringFormula.append("(");
                    stringFormula.append(FormatterUtil.formatFraction(fractions[j]));
                    stringFormula.append(")");

                } else {
                    stringFormula.append(FormatterUtil.formatFraction(fractions[j]));
                }
                if (j + 1 < fractions.length) {
                    stringFormula.append("\\times");
                }
            }

        }
        return stringFormula.toString();
    }

    private String formatSolution(String[] rows, boolean reduceRows){
        StringBuilder stringFormula = new StringBuilder();
        for (String row : rows) {
            String[] fractions = row.split(" ");
            for (int i = 0; i < 3; i++) {
                stringFormula.append(fractions[i]);
            }
            int i = 3;
            int vectorCount = 0;
            while (i < fractions.length){
                vectorCount++;
                for (int j = 0; j < 4; j++) {
                    stringFormula.append(fractions[i+j]);
                    if (j==0 && vectorCount==3 && reduceRows){
                        stringFormula.append("\\\\");
                    }
                }
                i+=4;
                i = formatVector(i, stringFormula, fractions);

            }
            stringFormula.append("\\\\");
        }

        return stringFormula.toString();
    }

    private String formatVectors(String[] rows){
        StringBuilder stringFormula = new StringBuilder();
        for (int i = 0; i < rows.length; i++) {
            String[] fractions = rows[i].split(" ");
            for (int j = 0; j < 3; j++) {
                stringFormula.append(fractions[j]);
            }
            formatVector(3, stringFormula, fractions);
            stringFormula.append("\\\\");
        }
        return  stringFormula.toString();
    }

    private int formatVector(int i, StringBuilder stringFormula, String[] fractions){
        stringFormula.append("\\begin{pmatrix}");
        while (i < fractions.length  && !fractions[i].matches(".*[a-zA-Z;]+.*")){
            stringFormula.append(FormatterUtil.formatFraction(fractions[i++]));
            stringFormula.append(" & ");
        }
        stringFormula.setLength(stringFormula.length()-2);
        stringFormula.append("\\end{pmatrix}");
        return i;
    }
}
