package gui.exercises;

import exercise.IExercise;
import gui.Main;
import gui.formatStep.IFormatter;
import gui.formatStep.FormatterFactory;
import io.github.egormkn.LatexView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;



public class ShowExerciseController extends Pane {


    @FXML
    private AnchorPane exerciseAnchorPane;

    @FXML
    private LatexView taskCanvas;

    @FXML
    private Button computeButton;

    @FXML
    private Button stepsButton;

    @FXML
    private Label taskLabel;

    @FXML
    private LatexView solutionCanvas;

    private IExercise exercise;
    private boolean solutionShowed = false;



    @FXML
    void computeButtonOnActoin(ActionEvent event) {
        if (solutionShowed){
            solutionCanvas.setVisible(false);
            solutionShowed = false;
            computeButton.setText("Zobrazit výsledek");
        }else{
            solutionCanvas.setVisible(true);
            solutionShowed = true;
            computeButton.setText("Skrýt výsledek");
        }

    }

    @FXML
    void stepsButtonOnAction(ActionEvent event) {
        Main.getMainController().setupExerciseSteps(exercise);
    }

    public void setExercise(IExercise exercise){
        this.exercise = exercise;
        setupEnvironment();
    }

    private void setupEnvironment(){
        solutionCanvas.setVisible(false);
        solutionShowed = false;
        taskLabel.setText(exercise.getTaskText());
        IFormatter formatter = FormatterFactory.getFormatter(exercise.getType());
        solutionCanvas.setFormula(formatter.formatSolution(exercise.getSolution()));
        taskCanvas.setFormula(formatter.format(exercise.getTask()));
        solutionCanvas.setSize(15);
        taskCanvas.setSize(15);
    }

}
